#include "main.ih"

int main() {
    ofstream one("one");
    ofstream two("two");

    BiStream bs(one, two);

    bs << "Hello world" << endl;

    return 0;
}