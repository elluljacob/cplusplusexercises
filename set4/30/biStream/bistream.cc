#include "biStream.hh"


BiStream::BiStream(std::ostream &out1, std::ostream &out2)
:
    std::ostream(&biStreamBuf),
    biStreamBuf(out1, out2)
{}
