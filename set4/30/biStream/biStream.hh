#ifndef MAIN_H
#define MAIN_H

#include "../biStreamBuffer/biStreamBuffer.hh"

class BiStream: public std::ostream 
{
    BiStreamBuffer biStreamBuf;

public:
    BiStream(std::ostream &out1, std::ostream &out2);
};

#endif