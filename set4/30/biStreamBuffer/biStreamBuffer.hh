#ifndef BI_STREAM_H
#define BI_STREAM_H

#include <iostream>

class BiStreamBuffer: public std::streambuf 
{
    std::ostream &out1;
    std::ostream &out2;
public:
    BiStreamBuffer(std::ostream &out1, std::ostream &out2);

protected:
    virtual int overflow(int num) override;
};

#endif
