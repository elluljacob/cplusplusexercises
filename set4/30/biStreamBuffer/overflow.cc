#include "biStreamBuffer.hh"

int BiStreamBuffer::overflow(int num) 
{
    if (num != traits_type::eof()) 
    {
        // Write to the first stream
        out1.put(num);
        // Write to the second stream
        out2.put(num);
    }
    return num;
}
