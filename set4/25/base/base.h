#ifndef BASE_H
#define BASE_H

#include <iosfwd>

class Base
{
    public:
        std::ostream &hello(std::ostream &out);
    
    private:
        virtual std::ostream &vHello(std::ostream &out) const;
};

#endif