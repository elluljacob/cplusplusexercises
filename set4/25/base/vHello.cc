#include "base.ih"

ostream &Base::vHello(ostream &out) const
{
    return out << "Hello from Base\n";
}