#ifndef MESSAGE_H
#define MESSAGE_H

#include "../base/base.h"

class Message
{
    Base &d_Base;

    public:
        Message(Base &base);
        void show() const;
};

#endif