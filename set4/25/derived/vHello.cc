#include "derived.ih"

ostream &Derived::vHello(ostream &out) const
{
    out << d_string << '\n';
    return out;
}