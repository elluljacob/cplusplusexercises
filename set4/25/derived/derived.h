#ifndef DERIVED_H
#define DERIVED

#include "../base/base.h"
#include <string>

class Derived: public Base  // We believe this space to improve readability
{
    std::string d_string;

    public:
        Derived(std::string const &input);

    private:
        std::ostream &vHello(std::ostream &out) const override;
};

#endif