#include "main.ih"

int main()
{
    Base baseClass;
    Derived derivedClass("Hello from Derived");

    Message m1(baseClass);
    Message m2(derivedClass);

    m1.show();
    m2.show();
}
