#ifndef MAIN_H
#define MAIN_H

class Basic
{
    public:
        Basic();
        Basic(int idx);
};

class Deriv1: public virtual Basic
{
    public: 
        Deriv1();
};

class Deriv2: public virtual Basic
{};

class Multi: public Deriv1, public Deriv2
{
    public:
        Multi();
};

#endif