#include "main.ih"

int main() 
try {
    RandBuffer randBuf(1, 23, 123321);
    istream randStream(&randBuf);

    for (int index = 0; index < 5; ++index) 
    { 
        string randNumber;
        getline(randStream, randNumber);  // Use getline to read from the stream
        cout << "Random Number: " << randNumber << '\n';
    }
}
catch (const exception& randBufferException) 
{
    cerr << "Error: " << randBufferException.what() << '\n';
}

