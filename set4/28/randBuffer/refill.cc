#include "randBuffer.ih"

// Method that fills the stream with a random number
void RandBuffer::refill()
{
    size_t randValue;
    do 
        randValue = random();
    while (randValue >= d_end);

    randValue = d_min + randValue % d_range;

    d_buffer = to_string(randValue) + '\n';  // Add newline to separate numbers
    setg(&d_buffer[0], &d_buffer[0], &d_buffer[0] + d_buffer.size());
}