#include "randBuffer.ih"

// Overrided underflow method
int RandBuffer::underflow()
{
    if (gptr() >= egptr())  // Buffer is empty, call refill
        refill();

    return static_cast<unsigned char>(*gptr());
}