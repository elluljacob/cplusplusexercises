#ifndef RAND_BUFFER_H
#define RAND_BUFFER_H

#include <streambuf>

class RandBuffer: public std::streambuf 
{
    int d_min;
    size_t d_range;
    size_t d_end;
    std::string d_buffer;

public:
    RandBuffer(int min, int max, size_t seed);
    virtual ~RandBuffer();

protected:
    int underflow() override;
    void refill();
};

#endif