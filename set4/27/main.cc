#include "main.ih"

int main()
{   //Handler and Processor both using Msg enum
    Handler handler;
    Processor processor;
    processor.process(MsgEnum::Msg::DEBUG);
    processor.process(MsgEnum::Msg::ALERT);
    handler.handle(MsgEnum::Msg::ALERT);
    handler.handle(MsgEnum::Msg::NONE);
}