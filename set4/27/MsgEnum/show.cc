#include "MsgEnum.ih"

void MsgEnum::show(Msg msg) //copied from exercise 6
{
    if (msg == Msg::NONE)
        cout << "NONE";
    else 
    {
        for (int idx= 0; idx < 7; ++idx) 
        {
            Msg flag = static_cast<Msg>(1 << idx);
            if (msg == flag) 
            {
                cout << msgNames[idx];
            }
        }
    }
    cout << '\n';
};