#include "MsgEnum.ih"

const char *MsgEnum::msgNames[] = 
{
    "DEBUG", "INFO", "NOTICE", "WARNING", "ERR", "CRIT", "ALERT", "EMERG"
};