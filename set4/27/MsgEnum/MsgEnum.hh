#ifndef INCLUDED_MSG_H_
#define INCLUDED_MSG_H_

class MsgEnum
{
    public:
        enum class Msg //Enum declared in base class
        {        
            NONE = 0,
            DEBUG   = 1 << 0,
            INFO    = 1 << 1,
            NOTICE  = 1 << 2,
            WARNING = 1 << 3,
            ERR     = 1 << 4,
            CRIT    = 1 << 5,
            ALERT   = 1 << 6,
            EMERG   = 1 << 7,
        };
    protected:
        void show(Msg msg); //show the message passed
        static const char *msgNames[];
};

#endif 
            