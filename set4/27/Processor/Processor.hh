#ifndef INCLUDED_PROCESSOR_H_
#define INCLUDED_PROCESSOR_H_

#include "../MsgEnum/MsgEnum.hh"

class Processor: private MsgEnum
{
    public:
        void process(Msg msg); //To show functionality
    private:
};

#endif
