#ifndef INCLUDED_HANDLER_H_
#define INCLUDED_HANDLER_H_

#include "../MsgEnum/MsgEnum.hh"

class Handler: private MsgEnum
{
    public:
        void handle(Msg msg); //To show functionality
    private:
};

#endif