#ifndef INCLUDED_FORK_H_
#define INCLUDED_FORK_H_

#include <unistd.h>

class Fork
{
    pid_t d_pid = ::fork(); //Initialises process ID

    public:
        void fork();
        virtual ~Fork();

    private:
        virtual void childprocess() = 0;
        virtual void parentProcess();

    protected:
        pid_t pid() const;
        int waitForChild() const;
};

#endif