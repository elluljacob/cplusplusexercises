#include "fork.ih"

void Fork::fork()
{
    if (d_pid == -1) //-1 means fork failed and return as is
    {
        cerr << "Fork failed." << '\n';
        return;
    }

    if (d_pid == 0) //the child process
    {
        childprocess();
        exit(0);
    } 
    else //the parent process
    {
        parentProcess();
        int exitCode = waitForChild();
        cout << "Child process exited with code: " << exitCode << '\n';
    }
}