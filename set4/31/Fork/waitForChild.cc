#include "fork.ih"

int Fork::waitForChild() const
{
    int status;
    
    waitpid(d_pid, &status, 0); //waits for child process
    
    return WEXITSTATUS(status);
}