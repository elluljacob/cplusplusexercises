#include "main.ih"


int main()
{
    // Use derivedFactory to create an array of pointers to Derived objects
    Base **bp = Base::derivedFactory(10);

    // Your code to use the array of pointers goes here
    for (size_t idx = 0; idx < 10; ++idx) 
    {
        // Use the pointers, for example, call show on each element
        Message m(bp[idx]);
        m.show();
    }

    // Deallocate the memory
    for (size_t idx = 0; idx < 10; ++idx)
        delete bp[idx];
    
    delete[] bp;
}
