#ifndef MESSAGE_H
#define MESSAGE_H

#include "../base/base.h"

class Message
{
    Base *d_Base; // Private member variable to store a pointer to Base

    public:
        Message(Base *base); // Constructor with a Base pointer parameter
        void show(); // Method to display the greeting using the stored Base pointer
};

#endif