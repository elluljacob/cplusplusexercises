#include "base.ih"

// Implementation of pure virtual function for returning an ostream
ostream &Base::vHello(ostream &out)
{
    return out << "Hello from Base\n";
}