#ifndef BASE_H
#define BASE_H

#include <iosfwd>

class Base
{
public:
    virtual ~Base(); // Virtual destructor 
    std::ostream &hello(std::ostream &out);
    // Static method to create an array of Base pointers
    static Base **derivedFactory(size_t size); 

private:
    // Pure virtual function for derived classes
    virtual std::ostream &vHello(std::ostream &out) = 0; 
};

#endif