#include "../base/base.ih"
#include "../derived/derived.h"

//Implementation of method to create an array of Base pointers
Base **Base::derivedFactory(size_t size)
{
    Base **array = new Base*[size];
    // Create size amount of new Derived objects
    for (size_t idx = 0; idx < size; ++idx) 
        array[idx] = new Derived();

    return array;
}
