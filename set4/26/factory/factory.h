#ifndef FACTORY_H
#define FACTORY_H

#include "../base/base.h"
#include <cstddef>

class Base
{
public:
    std::ostream &hello(std::ostream &out);
    // Static method to create an array of Base pointers
    static Base **derivedFactory(size_t size); 

private:
    virtual std::ostream &vHello(std::ostream &out);
};

#endif