#include "derived.ih"

// Implementation of the pure virtual function
ostream &Derived::vHello(ostream &out)
{
    return out << d_string << '\n';
}