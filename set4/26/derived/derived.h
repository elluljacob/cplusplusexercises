#ifndef DERIVED_H
#define DERIVED_H

#include "../base/base.h"
#include <string>

class Derived: public Base
{
    std::string d_string; // Private member variable for additional information

    public:
        Derived(); // Default constructor
        Derived(std::string input); // Parameterized constructor with string input

    private:
        std::ostream &vHello(std::ostream &out) override;
};

#endif