#include "workforce.ih"

void Workforce::processFile(string const &inputFile, 
    string const &outputFile)
{
    ifstream input(inputFile);
    ofstream output(outputFile);

    processStream(input, output);
}