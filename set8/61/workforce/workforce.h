#ifndef WORKFORCE_H
#define WORKFORCE_H

#include <string>
#include <iosfwd>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <atomic>

class Workforce
{
    std::vector<std::string> d_processedLines;
    std::mutex d_mutex;
    std::condition_variable d_condvar;
    std::atomic<bool> d_processingComplete;

    public:
        Workforce();
        static void runFile(Workforce* workforce, 
            std::string const &inputFile,
            std::string const &outputFile);
        static void runStream(Workforce* workforce, 
            std::istream &input,
            std::ostream &output);   
        void runAdmin(std::ostream &output);
        void completeProcessing();

    private:
        void processLine(std::string &line);
        void processFile(std::string const &inputFile, std::string const &outputFile);
        void processStream(std::istream &input, std::ostream &output);
};

#endif