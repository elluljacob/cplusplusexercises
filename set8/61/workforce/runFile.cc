#include "workforce.ih"

void Workforce::runFile(Workforce* workforce, 
    string const &inputFile,
    string const &outputFile)
{
    workforce->processFile(inputFile, outputFile);
}
