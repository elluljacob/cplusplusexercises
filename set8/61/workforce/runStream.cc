#include "workforce.ih"

void Workforce::runStream(Workforce* workforce, 
    istream &input,
    ostream &output)
{
    workforce->processStream(input, output);
}