#include "workforce.ih"

void Workforce::processStream(istream &input, ostream &output)
{
    string line;
    while (getline(input, line))
    {
        processLine(line);

        // Lock and update processed lines
        unique_lock<mutex> lock(d_mutex);
        d_processedLines.push_back(line);

        // Notify admin
        d_condvar.notify_one();
    }
}