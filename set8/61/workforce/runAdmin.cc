#include "workforce.ih"

void Workforce::runAdmin(ostream &output)
{
    while (true)
    {
        unique_lock<mutex> lock(d_mutex);
        d_condvar.wait(lock, [this] { return !d_processedLines.empty() || d_processingComplete; });

        if (d_processingComplete && d_processedLines.empty())
            break;

        string line = d_processedLines.back();
        d_processedLines.pop_back();
        output << line << endl;

        lock.unlock();
        d_condvar.notify_all();
    }
}