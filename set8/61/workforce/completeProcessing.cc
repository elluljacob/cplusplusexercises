#include "workforce.ih"

void Workforce::completeProcessing()
{
    d_processingComplete = true;
    d_condvar.notify_all();
}