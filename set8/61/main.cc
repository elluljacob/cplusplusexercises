#include "main.ih"

int main()
{
    Workforce workforce;

    thread workerFileThread1(Workforce::runFile, &workforce, "input.txt", "output1.txt");
    thread workerFileThread2(Workforce::runFile, &workforce, "input.txt", "output2.txt");

    thread adminfileThread(&Workforce::runAdmin, &workforce, std::ref(cout));

    workerFileThread1.join();
    workerFileThread2.join();

    workforce.completeProcessing();
    adminfileThread.join();
}