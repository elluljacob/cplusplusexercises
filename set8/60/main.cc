#include "main.ih"

// Thread function using rval ref as paramater
string threadFun(promise<string> &&prom)
{
    cerr << "entry\n";

    this_thread::sleep_for(chrono::seconds(5));
    cerr << "first cerr\n";

    this_thread::sleep_for(chrono::seconds(5));
    cerr << "second cerr\n";

    prom.set_value("end the program");
    return "end the program";
}

int main()
{
    promise<string> prom;
    future<string> fut = prom.get_future();

    thread t(threadFun, move(prom)); // Start thread

    size_t count = 0;
    while (fut.wait_for(chrono::seconds(0)) == future_status::timeout)
    {
        // do the main-task
        this_thread::sleep_for(chrono::seconds(1));
        cerr << "inspecting: " << ++count << '\n';
    }

    cerr << fut.get() << "\ndone\n";
    t.join(); // Wait for thread to finish
}
