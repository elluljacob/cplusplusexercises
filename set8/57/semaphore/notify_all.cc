#include "semaphore.ih"

void Semaphore::notify_all()
{
    unique_lock<mutex> lock(d_mutex);
    ++d_nAvailable;
    d_condition.notify_all();
}