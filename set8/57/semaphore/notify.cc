#include "semaphore.ih"

void Semaphore::notify()
{
    unique_lock<mutex> lock(d_mutex);
    ++d_nAvailable;
    d_condition.notify_one();
}