#include "semaphore.ih"

void Semaphore::wait()
{
    unique_lock<mutex> lock(d_mutex);
    // Wait until d_nAvailable is bigger than 0, meaning a worker would be available
    d_condition.wait(lock, [this] { return d_nAvailable > 0; });    
    --d_nAvailable;
}