#include "main.ih"

int main(int argc, char **argv)
try
{
    if (argc < 3)
        throw runtime_error("Insufficient arguments. Need at least 2 arguments.");

    string childPath = argv[1];
    int lines = stoi(argv[2]);
    int timeout = (argc > 3) ? stoi(argv[3]) : -1;

    ProgramRunner runner(childPath, lines, timeout);
    runner.run();

    if (!runner.timeoutOccurred())
        cout << "Program ended normally\n";
}
catch (exception const &e)
{
    cerr << "Error: " << e.what() << '\n';
}