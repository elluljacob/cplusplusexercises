#include "semaphore.ih"

bool Semaphore::wait_for(size_t seconds)
{
    unique_lock<mutex> lock(d_mutex);
    if (!d_nAvailable)
    {
        if (d_condition.wait_for(lock, chrono::seconds(seconds)) == cv_status::timeout)
            return false;
    }
    --d_nAvailable;
    return true;
}
