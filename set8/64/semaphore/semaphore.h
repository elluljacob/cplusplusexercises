#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#include <mutex>
#include <condition_variable>

class Semaphore
{
    size_t d_nAvailable;
    mutable std::mutex d_mutex;
    std::condition_variable d_condition;

public:
    Semaphore(size_t nAvailable);
    void notify();
    void notify_all();
    size_t size() const;
    void wait();
    bool wait_for(size_t seconds);
    void set(size_t nAvailable);
};

#endif