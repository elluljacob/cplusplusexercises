#include "ProgramRunner.ih"

ProgramRunner::~ProgramRunner()
{
    if (childPid > 0)
        kill(childPid, SIGTERM);
}