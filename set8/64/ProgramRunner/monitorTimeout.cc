#include "ProgramRunner.ih"

void ProgramRunner::monitorTimeout()
{
    if (!semaphore.wait_for(timeout))
    {
        cout << "Program ended at timeout\n";
        if (childPid > 0)
            kill(childPid, SIGTERM);
        timeoutFlag = true;
    }
}