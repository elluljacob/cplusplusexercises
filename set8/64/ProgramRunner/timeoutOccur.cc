#include "ProgramRunner.ih"

bool ProgramRunner::timeoutOccurred() const
{
    return timeoutFlag;
}