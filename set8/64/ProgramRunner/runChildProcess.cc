#include "ProgramRunner.ih"

void ProgramRunner::runChildProcess()
{
    childPid = fork();

    if (childPid == 0)
    {
        execl(childPath.c_str(), childPath.c_str(), to_string(lines).c_str(), (char *)NULL);
        cerr << "Failed to start child process\n";
        exit(1);
    }
    else if (childPid > 0)
    {
        int status;
        waitpid(childPid, &status, 0);
    }
    else
        cerr << "Failed to fork\n";
}