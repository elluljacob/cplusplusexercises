#include "ProgramRunner.ih"

ProgramRunner::ProgramRunner(string const &childPath, int lines, int timeout)
    : childPath(childPath),
      lines(lines),
      timeout(timeout),
      timeoutFlag(false),
      childPid(-1),
      semaphore(0)
{
}