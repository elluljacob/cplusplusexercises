#include "ProgramRunner.ih"

void ProgramRunner::run()
{
    thread childThread(&ProgramRunner::runChildProcess, this);

    thread timeoutThread;
    if (timeout != -1)
        timeoutThread = thread(&ProgramRunner::monitorTimeout, this);

    childThread.join();
    semaphore.notify();

    if (timeout != -1)
        timeoutThread.join();
}