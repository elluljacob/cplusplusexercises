#ifndef PROGRAM_RUNNER_H
#define PROGRAM_RUNNER_H

#include "../semaphore/semaphore.h"
#include <string>

class ProgramRunner
{
public:
    ProgramRunner(std::string const &childPath, int lines, int timeout);
    ~ProgramRunner();
    void run();
    bool timeoutOccurred() const;

private:
    std::string childPath;
    int lines;
    int timeout;
    bool timeoutFlag;
    pid_t childPid;
    Semaphore semaphore;

    void runChildProcess();
    void monitorTimeout();
};

#endif