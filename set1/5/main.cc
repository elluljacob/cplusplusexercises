#include "namespaces.h"

// void fun(First::Enum symbol) {
//     std::cout << "Global fun called" << std::endl;
// }

int main() 
{

    First::Enum symbol = First::Enum();
    fun(symbol);  // Calls First::fun

    // To call Second::fun, you need to use the Second namespace explicitly
    // Second::fun(symbol);

}