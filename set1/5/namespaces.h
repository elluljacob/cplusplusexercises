#ifndef NAMESPACES_H
#define NAMESPACES_H

#include <iostream>

namespace First 
{
    enum Enum {};
    void fun(First::Enum symbol) 
    {
        std::cout << "First::fun called" << std::endl;
    }
}

namespace Second 
{
    void fun(First::Enum symbol) 
    {
        std::cout << "Second::fun called" << std::endl;
    }
}

#endif
