#include "msg.h"
#include <iostream>

const char* msgNames[] = {
 "DEBUG", "INFO", "NOTICE", "WARNING", "ERR", "CRIT", "ALERT", "EMERG"
};

void show(Msg msg)
{
    std::cout << "Msg values:";

    if (msg == Msg::NONE)
        std::cout << " NONE";
    
    else
        for (size_t i = 0; i < sizeof(msgNames) / sizeof(msgNames[0]); ++i)
        {
            Msg flag = static_cast<Msg>(1 << i);
            if ((msg & flag) == flag)
            {
                std::cout << ' ';
                std::cout << msgNames[i];
            }
        }

    std::cout << '\n';
}