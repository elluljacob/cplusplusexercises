#ifndef INCLUDED_MSG_H_
#define INCLUDED_MSG_H_


enum class Msg
{        
    NONE = 0,
    DEBUG   = 1 << 0,
    INFO    = 1 << 1,
    NOTICE  = 1 << 2,
    WARNING = 1 << 3,
    ERR     = 1 << 4,
    CRIT    = 1 << 5,
    ALERT   = 1 << 6,
    EMERG   = 1 << 7,
    ALL     = (1 << 8) - 1
};
    
    Msg operator&(Msg lhs, Msg rhs)
    {
    return static_cast<Msg>(static_cast<int>(lhs) & static_cast<int>(rhs));
    }

    Msg operator|(Msg lhs, Msg rhs)
    {
    return static_cast<Msg>(static_cast<int>(lhs) | static_cast<int>(rhs));
    }

    Msg operator^(Msg lhs, Msg rhs)
    {
    return static_cast<Msg>(static_cast<int>(lhs) ^ static_cast<int>(rhs));
    }

    Msg operator~(Msg msg)
    {
    return static_cast<Msg>(~static_cast<int>(msg));
    }

    bool operator==(Msg lhs, Msg rhs)
    {
    return static_cast<int>(lhs) == static_cast<int>(rhs);
    }

    bool operator!=(Msg lhs, Msg rhs)
    {
    return static_cast<int>(lhs) != static_cast<int>(rhs);
    }

    bool operator!(Msg msg)
    {
    return msg == Msg::NONE;
    }

    void show(Msg msg);


#endif 
            