#include "Numbers.ih"

Numbers operator+(Numbers const &lhs, Numbers const &rhs)
{
    Numbers tmp { lhs };
    if (tmp.d_size != rhs.d_size)
    {
        std::cerr << "Error: sizes do not match";
        return lhs;
    }

    for (size_t idx=0; idx!=tmp.d_size; ++idx)
        tmp.d_nums[idx]+=rhs.d_nums[idx];

    return tmp;
}