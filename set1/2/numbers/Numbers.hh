#ifndef INCLUDED_NUMBERS_H_
#define INCLUDED_NUMBERS_H_

#include <iosfwd>
#include <functional>

class Numbers
{
    size_t d_size = 0;
    int *d_nums = 0;

    public:
        explicit Numbers(size_t size);
    
        Numbers(size_t size, int value);
        Numbers(size_t size, int *values);
        Numbers(std::initializer_list<int> iniList);
        Numbers(Numbers const &other);
        Numbers(Numbers &&tmp);
        ~Numbers();

        void swap(Numbers &other);
        Numbers &operator=(Numbers const &other);
        Numbers &operator=(Numbers &&tmp);

        Numbers &operator+=(Numbers const &rhs) &;
        Numbers operator+=(Numbers const &rhs) &&;

        Numbers &operator-=(Numbers const &rhs) &;
        Numbers operator-=(Numbers const &rhs) &&;

        Numbers &operator*=(int const &rhs) &;
        Numbers operator*=(int const &rhs) &&;

        Numbers &operator/=(int const &rhs) &;
        Numbers operator/=(int const &rhs) &&;

        friend Numbers operator+(Numbers const &lhs, Numbers const &rhs);
        friend Numbers operator+(Numbers const &&lhs, Numbers const &rhs);

        friend Numbers operator-(Numbers const &lhs, Numbers const &rhs);
        friend Numbers operator-(Numbers const &&lhs, Numbers const &rhs);

        friend Numbers operator*(Numbers const &lhs, int const rhs);
        friend Numbers operator*(Numbers const &&lhs, int const rhs);
        
        friend Numbers operator/(Numbers const &lhs, int const rhs);
        friend Numbers operator/(Numbers const &&lhs, int const rhs);

        // members of the public interface, like accessors
    
    private:
        // support members for this class, if any
};

#endif 