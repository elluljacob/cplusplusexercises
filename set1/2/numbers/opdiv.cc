#include "Numbers.ih"

Numbers operator/(Numbers const &lhs, int const rhs)
{
    Numbers tmp { lhs };
    for (size_t idx=0; idx!=tmp.d_size; ++idx)
        tmp.d_nums[idx]/=rhs;


    return tmp;
}