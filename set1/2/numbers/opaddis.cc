#include "Numbers.ih"

Numbers &Numbers::operator+=(Numbers const &rhs) &
{
    Numbers tmp{*this};

    if (tmp.d_size!=rhs.d_size)
    {
        std::cerr << "Error: sizes do not match";
        return *this;
    }

    for (size_t idx=0; idx!=tmp.d_size; ++idx)
        tmp.d_nums[idx]+=rhs.d_nums[idx];

    swap(tmp);

    return *this;
}