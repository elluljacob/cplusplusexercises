#include "Numbers.ih"

Numbers &Numbers::operator/=(int const &rhs) &
{
    if (rhs==0)
    {
        std::cerr << "Error: cannot divide by 0";
        return *this;
    }

    Numbers tmp {*this};

    for (size_t idx=0; idx!=tmp.d_size; ++idx)
        tmp.d_nums[idx]/=rhs;

    swap(tmp);

    return *this;
}