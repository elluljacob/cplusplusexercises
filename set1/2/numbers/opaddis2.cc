#include "Numbers.ih"

Numbers Numbers::operator+=(Numbers const &rhs) &&
{
    for (size_t idx=0; idx != (*this).d_size; ++idx)
        this->d_nums[idx] += rhs.d_nums[idx];

    return std::move(*this);
}