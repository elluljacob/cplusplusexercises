#include "Numbers.ih"

Numbers Numbers::operator*=(int const &rhs) &&
{
    Numbers tmp {*this};

    for (size_t idx=0; idx!=tmp.d_size; ++idx)
        tmp.d_nums[idx]*=rhs;
    
    swap(tmp);

    return std::move(*this);
}