#include "Numbers.h"
#include <algorithm>

// Constructors
Numbers::Numbers(size_t size) : d_size(size), d_nums(new int[size]) {}

Numbers::Numbers(size_t size, int value) : d_size(size), d_nums(new int[size])
{
    std::fill(d_nums, d_nums + size, value);
}

Numbers::Numbers(std::initializer_list<int> iniList) : d_size(iniList.size()), d_nums(new int[iniList.size()])
{
    std::copy(iniList.begin(), iniList.end(), d_nums);
}

Numbers::Numbers(size_t size, int *values) : d_size(size), d_nums(new int[size])
{
    std::copy(values, values + size, d_nums);
}

// Destructor
Numbers::~Numbers()
{
    delete[] d_nums;
}

// Index operators
int &Numbers::operator[](size_t index)
{
    if (index >= d_size)
    {
        throw std::out_of_range("Index out of range");
    }
    return d_nums[index];
}

int const &Numbers::operator[](size_t index) const
{
    if (index >= d_size)
    {
        throw std::out_of_range("Index out of range");
    }
    return d_nums[index];
}

// Insertion operator
std::ostream &operator<<(std::ostream &out, Numbers const &nums)
{
    for (size_t i = 0; i < nums.d_size; ++i)
    {
        out << nums.d_nums[i] << " ";
    }
    return out;
}

// Comparison operators
bool Numbers::operator==(Numbers const &other) const
{
    if (d_size != other.d_size)
    {
        return false;
    }

    return std::equal(d_nums, d_nums + d_size, other.d_nums);
}

bool Numbers::operator!=(Numbers const &other) const
{
    return !(*this == other);
}
