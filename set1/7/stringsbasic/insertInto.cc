#include "strings.ih"

// Private member function for insertion
std::ostream &Strings::insertInto(std::ostream &out) const 
{
    for (size_t idx = 0; idx != d_size; ++idx) 
    {
        if (d_manip) 
            d_manip(out, *this, idx);
        else 
        {
            out << d_str[idx];
            if (idx + 1 != d_size) 
                out << d_sep;           
        }
    }

    // Reset manipulators to default after use
    d_manip = nullptr;
    d_sep = "\n";

    return out;
}

