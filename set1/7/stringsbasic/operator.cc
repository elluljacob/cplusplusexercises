#include "strings.ih"

// Overloaded operator<<
std::ostream &operator<<(std::ostream &out, Strings const &str) 
{
    return str.insertInto(out);
}


// Overloaded function call operator for separator
Strings const &Strings::operator()(char const *sep) 
{
    d_sep = sep;
    return *this;
}

Strings const &Strings::operator()(void (*manip)(std::ostream &, Strings const &, size_t)) 
{
    d_manip = manip;
    return *this;
}
