#ifndef INCLUDED_STRINGS_
#define INCLUDED_STRINGS_

#include <iostream>
#include <string>

class Strings
{
    size_t d_size = 0;
    std::string *d_str = 0;

    
    mutable std::string d_sep = "\n"; // Default separator
    mutable void (*d_manip)(std::ostream &, Strings const &, size_t) = nullptr;

    public:
        Strings() = default;
        Strings(int argc, char *argv[]);    // 2.cc
        Strings(char *environLike[]);       // 3.cc

        Strings(Strings const &other);      // see part 1: allocation
        Strings(Strings &&tmp);             // see part 1: allocation

        ~Strings();

        Strings &operator=(Strings const &rhs); // see part 1: allocation
        Strings &operator=(Strings &&rhs);      // see part 1: allocation

        void swap(Strings &other);              

        size_t size() const;
        std::string const &at(size_t idx) const;
        std::string &at(size_t idx);

        void add(std::string const &next);          // add another element
        
        // Manipulator functions
        Strings const &operator()(char const *sep);
        Strings const &operator()(void (*manip)(std::ostream &, Strings const &, size_t));

        friend std::ostream &operator<<(std::ostream &out, Strings const &str);

    private:
        std::string *duplicateAndEnlarge();

        // Private member function for insertion
        std::ostream &insertInto(std::ostream &out) const;

        // Standard insertion function
        static void stdInsertion(std::ostream &out, Strings const &str, size_t idx);

};

inline size_t Strings::size() const         // potentially dangerous practice:
{                                           // inline accessors
    return d_size;
}

inline std::string const &Strings::at(size_t idx) const
{
    return d_str[idx];
}

inline std::string &Strings::at(size_t idx)
{
    return d_str[idx];
}

        
#endif

