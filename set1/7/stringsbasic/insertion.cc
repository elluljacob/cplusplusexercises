#include "strings.ih"

// Standard insertion function
void Strings::stdInsertion(std::ostream &out, Strings const &str, size_t idx)
{
    out << str.at(idx) << '\n';
}