#include "main.ih"

bool promptGet(istream &in, string &str)
{
    cout << "Enter a line or ^D\n";
    return bool(getline(in, str));
}