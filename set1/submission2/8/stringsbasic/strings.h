#ifndef INCLUDED_STRINGS_
#define INCLUDED_STRINGS_

#include <string>
#include <iosfwd>   // std::ostream

class Strings
{
    size_t d_size = 0;
    std::string *d_str = 0;
    size_t *d_shared = nullptr;
    std::string *d_tempStr = nullptr;       // TODO these are too complex?
    size_t *d_tempSize = nullptr;

    public:
        class Proxy
        {
            friend class Strings;           // We do need this in at1.cc

            Strings &d_parent;
            size_t d_index;

            public:
                operator const std::string() const;             // proxyString.cc
                Proxy& operator=(std::string const &value);     // proxyEquals.cc
                Proxy& operator=(Proxy const &other);           // proxyEquals2.cc
                friend std::ostream &operator<<(std::ostream &os, Proxy const &proxy);

            private:
                Proxy(Strings& parent, size_t index);
        };
        Strings() = default;
        Strings(int argc, char *argv[]);    // 2.cc
        Strings(char *environLike[]);       // 3.cc

        Strings(Strings const &other);      // see part 1: allocation
        Strings(Strings &&tmp);             // see part 1: allocation

        ~Strings();

        Strings &operator=(Strings const &rhs); // see part 1: allocation
        Strings &operator=(Strings &&rhs);      // see part 1: allocation

        void swap(Strings &other);              

        size_t size() const;

        Proxy operator[](size_t index);                         // at1.cc
        std::string const &operator[](size_t index) const;      // at2.cc

        Strings& operator+=(std::string const &next);

    private:
        std::string *duplicateAndEnlarge();

        void cow();
        void commit();      // TODO this is not indended
        void rollBack();
};

inline size_t Strings::size() const         // potentially dangerous practice:
{                                           // inline accessors
    return d_size;
}
        
#endif

