#include "strings.ih"

void Strings::rollBack()
{
    delete[] d_tempStr;
    delete d_tempSize;

    d_tempStr = nullptr;
    d_tempSize = nullptr;
}