#include "strings.ih"

Strings::Proxy Strings::operator[](size_t index)
{
    return Proxy(*this, index);
}