#include "main.ih"

void only2(std::ostream &out, Strings const &str, size_t idx)
{
    if (idx < 2)
        out << str.at(idx) << (idx == 0 ? ", and: " : "");
}