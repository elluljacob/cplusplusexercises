#include "strings.ih"

// Overloaded operator<<
std::ostream &operator<<(std::ostream &out, Strings const &str) 
{
    return str.insertInto(out);
}

