#include "strings.ih"

// Overloaded function call operator for separator
Strings const &Strings::operator()(char const *sep) 
{
    d_sep = sep;
    return *this;
}