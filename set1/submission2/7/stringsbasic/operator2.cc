#include "strings.ih"

Strings const &Strings::operator()(void (*manip)(std::ostream &, Strings const &, size_t)) 
{
    d_manip = manip;
    return *this;
}