#include "numbers.h"

int const &Numbers::operator[](size_t index) const
{
    if (index >= d_size)
        throw std::out_of_range("Index out of range");
    
    return d_nums[index];
}