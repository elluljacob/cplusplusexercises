#include "numbers.h"

bool operator!=(Numbers const &lhs, Numbers const &rhs)
{
    return !(lhs == rhs);
}