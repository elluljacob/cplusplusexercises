#include "numbers.h"

// Index operators
int &Numbers::operator[](size_t index)
{
    if (index >= d_size)
        throw std::out_of_range("Index out of range");

    return d_nums[index];
}