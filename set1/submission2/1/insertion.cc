#include <iostream>
#include <string>

struct Numbers
{
    int *d_nums;
    int d_size;
};

std::ostream &operator<<(std::ostream &out, Numbers const &nums)
{
    for (int idx = 0; idx < nums.d_size; ++idx)
        out << nums.d_nums[idx] << std::string(" ");
    
    return out;
}
