#ifndef INCLUDED_NUMBERS_H_
#define INCLUDED_NUMBERS_H_

#include <iosfwd>
#include <initializer_list>
#include <stdexcept>

class Numbers
{

    friend std::ostream& operator<<(std::ostream &out, Numbers const &nums);
    friend std::ostream& operator<<(std::ostream &out, int num);


    friend bool operator==(Numbers const &lhs, Numbers const &rhs);
    friend bool operator!=(Numbers const &lhs, Numbers const &rhs);

    size_t d_size = 0;
    int *d_nums = nullptr;

    public:
        explicit Numbers(size_t size);
        
        Numbers(size_t size, int value);
        Numbers(size_t size, int *values);
        Numbers(std::initializer_list<int> iniList);
        Numbers(Numbers const &other);
        Numbers(Numbers &&tmp);
        ~Numbers();

        void swap(Numbers &other);
        Numbers &operator=(Numbers const &other);
        Numbers &operator=(Numbers &&tmp);

        // Index operators
        int &operator[](size_t index);
        int const &operator[](size_t index) const;

    private:
        // Support members for this class, if any
        
};

#endif