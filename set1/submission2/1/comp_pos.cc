#include "numbers.h"

bool operator==(Numbers const &lhs, Numbers const &rhs)
{
    if (lhs.d_size != rhs.d_size)
        return false; // Different sizes, not equal

    for (size_t idx = 0; idx < lhs.d_size; ++idx)
        if (lhs[idx] != rhs[idx])
            return false; // If any pair of entries is not equal, return false
        

    return true; // All entries are equal
}
