#include "msg.h"

const char* msgNames[] = {
 "DEBUG", "INFO", "NOTICE", "WARNING", "ERR", "CRIT", "ALERT", "EMERG"
};

void show(Msg msg)
{
    std::cout << "Msg values:";

    if (msg == Msg::NONE)
        std::cout << " NONE";
    
    else
        for (size_t idx = 0; idx != sizeof(msgNames) / sizeof(msgNames[0]); ++idx)
        {
            Msg flag = Msg(1 << idx);
            if (static_cast<int>(msg & flag))
            {
                std::cout << ' ';
                std::cout << msgNames[idx];
            }
        }

    std::cout << '\n';
}
