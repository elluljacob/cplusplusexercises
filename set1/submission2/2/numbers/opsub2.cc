#include "Numbers.ih"

Numbers operator-(Numbers const &&lhs, Numbers const &rhs)
{
    if (lhs.d_size != rhs.d_size)
    {
        std::cerr << "Error: sizes do not match";
        return lhs;
    }

    for (size_t idx = 0; idx != lhs.d_size; ++idx)
        lhs.d_nums[idx] -= rhs.d_nums[idx];

    return lhs;
}