#include "Numbers.ih"

Numbers operator-(Numbers const &lhs, Numbers const &rhs)
{
    if (lhs.d_size != rhs.d_size)
    {
        std::cerr << "Error: sizes do not match";
        return lhs;
    }

    Numbers newVal { lhs };
    for (size_t idx = 0; idx != newVal.d_size; ++idx)
        newVal.d_nums[idx] -= rhs.d_nums[idx];

    return newVal;
}