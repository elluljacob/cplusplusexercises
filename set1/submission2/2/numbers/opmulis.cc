#include "Numbers.ih"

Numbers &Numbers::operator*=(int const &rhs)
{
    for (size_t idx = 0; idx != (*this).d_size; ++idx)
        (*this).d_nums[idx] *= rhs;

    return *this;
}