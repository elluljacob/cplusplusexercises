#include "Numbers.ih"

Numbers Numbers::operator/=(int const &&rhs)
{
    if (rhs == 0)
    {
        std::cerr << "Error: cannot divide by 0";
        return *this;
    }

    for (size_t idx = 0; idx != (*this).d_size; ++idx)
        (*this).d_nums[idx] /= rhs;

    return *this;
}