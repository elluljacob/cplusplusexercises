#include "Numbers.ih"

Numbers operator/(Numbers const &&lhs, int const rhs)
{
    for (size_t idx = 0; idx != lhs.d_size; ++idx)
        lhs.d_nums[idx] /= rhs;

    return lhs;
}