#include "Numbers.ih"

Numbers Numbers::operator+=(Numbers const &&rhs)
{
    if ((*this).d_size != rhs.d_size)
    {
        std::cerr << "Error: sizes do not match";
        return *this;
    }

    for (size_t idx=0; idx != (*this).d_size; ++idx)
        this->d_nums[idx] += rhs.d_nums[idx];

    return *this;
}