#include <filesystem>

namespace fs = std::filesystem;

int main(int argc, char *argv[]) 
{
    const fs::path sourcePath = argv[1];
    const fs::path destinationPath = argv[2];

    // Copy the file using Filesystem library
    fs::copy_file(sourcePath, destinationPath, fs::copy_options::update_existing);
    
    // Set the same timestamp for the destination file
    auto source_time = fs::last_write_time(sourcePath);
    fs::last_write_time(destinationPath, source_time);
}
