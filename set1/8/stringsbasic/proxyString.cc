#include "strings.ih"

// We define std:: namespace on the string so the definition and declaration are the same, otherwise IDE complains
Strings::Proxy::operator std::string() const    
{
    return d_parent.d_str[d_index];
}