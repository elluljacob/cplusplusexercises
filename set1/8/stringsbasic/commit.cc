#include "strings.ih"

void Strings::commit()
{
    delete[] d_str;
    d_str = d_tempStr;
    d_size = *d_tempSize;
    d_tempStr = nullptr;
    d_tempSize = nullptr;
}