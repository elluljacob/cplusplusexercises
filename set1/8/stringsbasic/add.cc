#include "strings.ih"

Strings& Strings::operator+=(std::string const &next)
{
    string *newStr = duplicateAndEnlarge();

    for (size_t idx = 0; idx != d_size; ++idx)
        newStr[idx] = move(d_str[idx]);

    newStr[d_size] = next;

    delete[] d_str;

    ++d_size;
    d_str = newStr;

    return *this;
}
