#include "strings.ih"

void Strings::cow()
{
    if (d_shared && *d_shared > 1)
    {
        --(*d_shared);

        d_tempSize = new size_t(d_size);
        d_tempStr = duplicateAndEnlarge();
        d_shared = new size_t(1);

        for (size_t idx = 0; idx != d_size; ++idx)
            d_tempStr[idx] = d_str[idx];
    }
    else if (d_shared && *d_shared == 1 && d_tempStr)
        rollBack();
}