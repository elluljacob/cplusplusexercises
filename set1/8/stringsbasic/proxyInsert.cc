#include "strings.ih"

ostream &operator<<(ostream &os, Strings::Proxy const &proxy)
{
    os << static_cast<string>(proxy);
    return os;
}