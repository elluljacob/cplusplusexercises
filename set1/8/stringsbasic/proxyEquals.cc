#include "strings.ih"

Strings::Proxy& Strings::Proxy::operator=(string const &value)
{
    if (d_parent.d_shared && *d_parent.d_shared > 1)
    {
        --(*d_parent.d_shared);

        d_parent.d_shared = new size_t(1);
        d_parent.d_str = d_parent.duplicateAndEnlarge();
    }

    d_parent.d_str[d_index] = value;
    return *this;
}