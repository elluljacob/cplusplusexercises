#include <cstdio>     
#include <fcntl.h>     
#include <unistd.h>  
#include <sys/stat.h>  

int main(int argc, char* argv[]) 
{
    // Open the source file for reading only
    int src = open(argv[1], O_RDONLY);

    // Open the destination file for writing; create it if it doesn't exist
    int dest = open(argv[2], O_WRONLY | O_CREAT, 0666);

    char buffer[BUFSIZ];
    ssize_t bytesRead;

    // Read data from source and write to destination until there's no more data
    while ((bytesRead = read(src, buffer, BUFSIZ)) > 0) 
        write(dest, buffer, bytesRead);

    close(src);
    close(dest);

    // Get metadata of the source file
    struct stat statBuf;
    stat(argv[1], &statBuf);

    // Update the timestamps of the destination file to match the source file
    struct timespec times[2] = {statBuf.st_atim, statBuf.st_mtim};
    utimensat(AT_FDCWD, argv[2], times, 0);
}
