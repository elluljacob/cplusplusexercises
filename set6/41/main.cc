#include "main.ih"

int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        cerr << "Usage: " << argv[0] << " <numValues> <minValue> <maxValue>\n";
        return 1;
    }

    try
    {
        size_t const numValues = stoul(argv[1]);
        size_t const minValue = stoul(argv[2]);
        size_t const maxValue = stoul(argv[3]);

        FillUnique filler(minValue, maxValue);
        std::vector<size_t> values(numValues);
        filler.fill(values);

        copy(values.begin(), values.end(), ostream_iterator<size_t>(cout, " "));
        cout << '\n';

    }
    catch (std::exception const &error)
    {
        cerr << "Invalid argument: " << error.what() << "\n";
        return 1;
    }
}