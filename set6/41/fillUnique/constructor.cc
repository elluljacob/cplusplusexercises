#include "fillUnique.ih"
// Initializes engine with time 0
std::mt19937 Random::engine(static_cast<unsigned>(time(0)));

FillUnique::FillUnique(size_t minValue, size_t maxValue)
: 
    d_random(minValue, maxValue)
{
}