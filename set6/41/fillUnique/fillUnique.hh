#ifndef FILL_UNIQUE_H
#define FILL_UNIQUE_H

#include "../random/random.hh"

class FillUnique 
{
    private:
        size_t storeValue(std::vector<size_t> values);
        Random d_random;

    public:
        FillUnique(size_t minValue, size_t maxValue);
        void fill(std::vector<size_t> &values);
};
#endif