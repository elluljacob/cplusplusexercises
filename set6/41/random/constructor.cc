#include "random.ih"

Random::Random(size_t min, size_t max) 
{
    if (min > max)
    //uses invalid_argument to pass a message on catch
        throw std::invalid_argument("Min value must be <= to max value.");
    range = max - min + 1;
    distribution = std::uniform_int_distribution<size_t>(min, max);
}