#include "fieldVect.ih"

// Comparator using case insensitive comparison
bool compareEntries(FieldVect const &nameA, FieldVect const &nameB)
{
    if (nameA.first != nameB.first)
        return nameA.first > nameB.first; // Latest date first

    string aNameLower, bNameLower;
    transform(nameA.second.begin(), nameA.second.end(), back_inserter(aNameLower), ::tolower);
    transform(nameB.second.begin(), nameB.second.end(), back_inserter(bNameLower), ::tolower);

    return aNameLower < bNameLower;
}
