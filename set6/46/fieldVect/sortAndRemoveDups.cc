#include "fieldVect.ih"

Fields sortAndRemoveDuplicates(Fields &fields)
{
    sort(fields.begin(), fields.end(), compareEntries);
    Fields originalFields = fields;

    auto newEnd = unique(fields.begin(), fields.end(), isDuplicate);

    // Identify the removed entries by comparing sorted original with unique result
    Fields removedEntries;
    auto itOriginal = originalFields.begin();
    auto itUnique = fields.begin();

    while (itOriginal != originalFields.end())
    {
        if (itUnique == newEnd || *itOriginal != *itUnique) // If end of unique reached or elements differ
            removedEntries.push_back(*itOriginal);          // This entry was removed
        else
            ++itUnique; // This entry was not removed, move to next unique

        ++itOriginal; // Always move to next original
    }
    fields.erase(newEnd, fields.end()); // Erase the duplicates from the original vector

    return removedEntries;
}
