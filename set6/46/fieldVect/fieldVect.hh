#ifndef FIELD_VECT_
#define FIELD_VECT_

#include <vector>
#include <string>

// Hold each record's date and name using std::pair
using FieldVect = std::pair<std::string, std::string>;
using Fields = std::vector<FieldVect>;

bool compareEntries(FieldVect const &a, FieldVect const &b);
bool isDuplicate(FieldVect const &a, FieldVect const &b);
Fields sortAndRemoveDuplicates(Fields &fields);
Fields readCSV(std::string const &filename);

#endif
