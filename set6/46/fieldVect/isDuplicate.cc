#include "fieldVect.ih"

// Comparator for identifying duplicates
bool isDuplicate(FieldVect const &nameA, FieldVect const &nameB)
{
    return nameA.first == nameB.first && nameA.second == nameB.second;
}