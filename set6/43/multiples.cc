#include "main.ih"

size_t multiples(Contr const &vs)
{
    size_t count = 0;

    for (Contr::const_iterator itr = adjacent_find(vs.begin(), vs.end()); 
        itr != vs.end();
        itr = adjacent_find(itr + 1, vs.end()))
            ++count;
    return count;
}