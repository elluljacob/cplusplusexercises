#include "Interactive.ih"

void Interactive::read()
{
    ifstream file(filename);
    if (!file.is_open())
    {
        cerr << "Error opening file: " << filename << '\n';
        return;
    }

    // Use a generic algorithm to fill the unordered map
    string domain;
    Fields fields;
    while (file >> domain >> fields.numFakeMails >> 
        fields.lastFakeMailTime >> fields.lastFakeMailDate)
    {
        dataMap[domain] = fields;

        // Consume newline character
        file.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    file.close();
}
