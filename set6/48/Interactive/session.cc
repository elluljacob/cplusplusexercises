#include "Interactive.ih"

void Interactive::session()
{
    unordered_map<string, Fields> wipMap = dataMap;

    while (true)
    {
        // Request input
        cout << "Enter a date or minimum number of received fake e-mails (or empty line to end): ";
        string input;
        getline(cin, input);

        if (input.empty())
            break;

        istringstream iss(input);
        int minNumFakeMails;

        if (regex_match(input, regex("\\b\\d{2}-\\d{2}-\\d{2}\\b")))
        {
            // Keep only entries with the specified date
            unordered_map<string, Fields> tempMap;
            for (auto const &entry : wipMap)
                if (entry.second.lastFakeMailDate == input)
                    tempMap[entry.first] = entry.second;

            wipMap = tempMap;
        }
        else if (iss >> minNumFakeMails)
        {
            // Keep only entries with at least the specified number of fake e-mails
            unordered_map<string, Fields> tempMap;
            for (const auto &entry : wipMap)
            {
                if (entry.second.numFakeMails >= minNumFakeMails)
                    tempMap[entry.first] = entry.second;
            }
            wipMap = tempMap;
        }

        // Show kept elements
        cout << "Kept elements:\n";
        for (auto const &entry : wipMap)
        {
            cout << entry.first << ": "
                 << entry.second.numFakeMails << " "
                 << entry.second.lastFakeMailTime << " "
                 << entry.second.lastFakeMailDate << endl;
        }

        // Show the number of kept elements
        cout << "Number of kept elements: " << wipMap.size() << "\n\n";
    }
}
