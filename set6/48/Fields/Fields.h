#ifndef FIELDS_H
#define FIELDS_H

#include <string>

struct Fields
{
    int numFakeMails;
    long lastFakeMailTime;
    std::string lastFakeMailDate;
};

#endif
