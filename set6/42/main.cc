#include <algorithm>
#include <cstring>
#include <iostream>
#include <iterator>

using namespace std;

int main(int argc, char *argv[])
{
    // Sort in ascending order and print
    sort(argv + 1, argv + argc, less<string>());
    copy(argv + 1, argv + argc, ostream_iterator<char *>(cout, "\n"));

    cout << '\n'; // Newline between sorted outputs

    // Sort in descending order and print
    sort(argv + 1, argv + argc, greater<string>());
    copy(argv + 1, argv + argc, ostream_iterator<char *>(cout, "\n"));
}
