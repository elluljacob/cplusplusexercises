#include "main.ih"

int main()
{
    vector<unsigned> vec(10);
    iota(vec.begin(), vec.end(), 0);
    transform(vec.begin(), vec.end(), vec.begin(), [](unsigned value) { return value * 2; });

    // Separate loop to keep implementation and display separate
    for (unsigned &val : vec)
        cout << val << " ";
    cout << '\n';
}
