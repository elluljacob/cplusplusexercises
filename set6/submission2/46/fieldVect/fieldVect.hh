#ifndef FIELD_VECT_
#define FIELD_VECT_

#include <vector>
#include <string>

// Hold each record's date and name using std::pair
using FieldPair = std::pair<std::string, std::string>;
using Fields = std::vector<FieldPair>;

bool isLaterOrCaseInsensitive(FieldPair const &entry1, FieldPair const &entry2);
bool isDuplicate(FieldPair const &entry1, FieldPair const &entry2);

void sortFields(Fields &fields);
Fields removeDuplicates(Fields &fields);
Fields readCSV(std::string const &filename);

#endif
