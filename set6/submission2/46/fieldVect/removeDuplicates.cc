#include "fieldVect.ih"

Fields removeDuplicates(Fields &fields)
{
    Fields removedEntries;
    auto it = unique(fields.begin(), fields.end(),
                     [&](FieldPair const &entry1, FieldPair const &entry2)
                     {
                         if (isDuplicate(entry1, entry2))
                         {
                             removedEntries.push_back(entry1);
                             return true;
                         }
                         return false;
                     });

    fields.erase(it, fields.end()); // Erase the duplicates from the original vector
    return removedEntries;
}