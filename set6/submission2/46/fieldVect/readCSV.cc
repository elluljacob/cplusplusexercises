#include "fieldVect.ih"

// reads the provided csv and returns the names and date as Fields
Fields readCSV(string const &filename)
{
    Fields fields;
    ifstream file(filename);
    string line;

    while (getline(file, line))
    {
        stringstream linestream(line);
        string date, name;

        getline(linestream, date, ','); // Get date
        // Skip to the 4th field
        for (size_t index = 0; index != 3; ++index)
            linestream.ignore(numeric_limits<streamsize>::max(), ',');

        getline(linestream, name, ','); // Get name

        fields.emplace_back(date, name);
    }
    return fields;
}
