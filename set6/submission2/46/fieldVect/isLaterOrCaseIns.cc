#include "fieldVect.ih"

// Comparator using case insensitive comparison (after it compares date)
bool isLaterOrCaseInsensitive(FieldPair const &entry1, FieldPair const &entry2) // really struggled with what to name this
{
    if (entry1.first != entry2.first)
        return entry1.first > entry2.first; // Latest date first

    // Use strcasecmp for case-insensitive comparison of names.
    return strcasecmp(entry1.second.c_str(), entry2.second.c_str()) < 0;
}
