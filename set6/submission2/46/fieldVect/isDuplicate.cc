#include "fieldVect.ih"

// Comparator for identifying duplicates
bool isDuplicate(FieldPair const &entry1, FieldPair const &entry2)
{
    return entry1.first == entry2.first && entry1.second == entry2.second;
}