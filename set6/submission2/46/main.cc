#include "main.ih"

int main()
{
    Fields fields = readCSV("data.csv");

    cout << "Original number of elements: " << fields.size() << '\n';

    cout << "Removed duplicated elements: \n";

    sort(fields.begin(), fields.end(), isLaterOrCaseInsensitive);
    Fields removedEntries = removeDuplicates(fields);
    for (auto const &removedEntry : removedEntries)
        cout << "Date: " << removedEntry.first << ", Name: " << removedEntry.second << '\n';

    cout << "Final number of elements: " << fields.size() << '\n';
    for (auto const &entry : fields)
        cout << "Date: " << entry.first << ", Name: " << entry.second << '\n';
}
