#include "fillUnique.ih"
#include <iostream>

size_t FillUnique::storeValue(std::vector<size_t> values)
{
    //only returns a value that is not already in the values vector
    size_t value;
    do 
        value = d_random();
    while (find(values.begin(), values.end(), value) != values.end());
    return value;
}