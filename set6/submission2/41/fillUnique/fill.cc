#include "fillUnique.ih"

void FillUnique::fill(std::vector<size_t> &values)
{
    if (d_random.getRange() < 2 * values.size())
        throw std::invalid_argument("Range must be >= 2x amount to fill.");

    // uses the generic algorithm generate to fill values
    generate(values.begin(), values.end(),
             [this, &values]()
             {
                 return storeValue(values);
             });
}