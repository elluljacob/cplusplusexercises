#ifndef RANDOM_H
#define RANDOM_H

#include <random>
class Random 
{
    private:
        static std::mt19937 engine;
        size_t range;
        std::uniform_int_distribution<size_t> distribution;
        
    public:
        Random(size_t min, size_t max);
        size_t getRange();
        size_t operator()();

};

inline size_t Random::getRange()
{
    return range;
}

inline size_t Random::operator()() 
{
    return distribution(engine);
}
#endif 