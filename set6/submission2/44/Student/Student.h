#ifndef STUDENT_H
#define STUDENT_H

#include <fstream>
#include <sstream>
#include <string>
#include <vector>

class Student
{
    public:
        static std::vector<Student> read(std::ifstream &inputFile);
        friend void writeNames(std::ostream &os, std::vector<Student> const &students);
        friend void writeNrs(std::ostream &os, std::vector<Student> const &students);

    private:
        double d_examGrade;
        std::string d_firstName;
        std::string d_lastName;
        std::string d_studentNumber;
        
};

#endif
