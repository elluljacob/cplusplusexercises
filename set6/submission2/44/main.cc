#include "main.ih"

int main(int argc, char *argv[])
{

    string const filename = argv[1];
    ifstream inputFile(filename);

    // Read and print all data
    vector<Student> const students = Student::read(inputFile);

    writeNames(cout, students);

    writeNrs(cout, students);
}
