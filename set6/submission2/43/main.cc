#include "main.ih"

int main()
{
    Contr vs{ 1, 2, 2, 3, 4, 4, 4, 5 };     // We changed this to contr to improve adaptability
    cout << multiples(vs) << " multiples were found\n";
}
