#ifndef INTERACTIVE_H
#define INTERACTIVE_H

#include "../Fields/Fields.h"

#include <unordered_map>

class Interactive
{
    private:
        std::unordered_map<std::string, Fields> dataMap;
        std::string filename;

    public:
        Interactive(std::string const &filename);
        void read();
        void session();
};

#endif
