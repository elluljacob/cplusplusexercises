#include "main.ih"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cerr << "Usage: " << argv[0] << " <filename>" << endl;
        return 1;
    }

    const string filename = argv[1];
    ifstream inputFile(filename);

    if (!inputFile)
    {
        cerr << "Error opening the file: " << filename << endl;
        return 1;
    }

    // Read and print all data
    vector<Student> students = Student::read(inputFile);

    writeNames(cout, students);

    writeNrs(cout, students);
}
