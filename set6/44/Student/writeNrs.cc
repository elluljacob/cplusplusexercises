#include "Student.ih"

void writeNrs(ostream &os, vector<Student> const &students)
{
    vector<size_t> indices(students.size());
    iota(indices.begin(), indices.end(), 0);

    // Sort indices based on student number
    sort(indices.begin(), indices.end(), [&students](size_t alpha, size_t beta)
         { return students[alpha].d_studentNumber < students[beta].d_studentNumber; });

    // Output the sorted data
    for (size_t index : indices)
    {
        const Student &student = students[index];
        os << "First Name: " << student.d_firstName << "\n";
        os << "Last Name: " << student.d_lastName << "\n";
        os << "Student Number: " << student.d_studentNumber << "\n";
        os << "Exam Grade: " << student.d_examGrade << "\n";
        os << "-----------------------------\n";
    }
}
