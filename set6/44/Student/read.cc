#include "Student.ih"

vector<Student> Student::read(ifstream &inputFile)
{
    vector<Student> students;

    string line;
    while (getline(inputFile, line)) // Read a line from the file
    {
        istringstream iss(line);
        vector<string> elements(istream_iterator<string>{iss},
                                istream_iterator<string>());

        size_t numElements = elements.size();

        if (numElements >= 4)
        {
            Student student;
            student.d_firstName.clear();
            student.d_lastName = elements[numElements - 3];
            student.d_studentNumber = elements[numElements - 2];
            student.d_examGrade = stod(elements[numElements - 1]);

            // Prefix additional elements to first name
            for (size_t idx = 0; idx < numElements - 3; ++idx)
                student.d_firstName += elements[idx] + " ";

            // Store the student data in the vector
            students.push_back(student);
        }
    }

    return students;
}
