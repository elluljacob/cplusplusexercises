#ifndef STUDENT_H
#define STUDENT_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

class Student
{
public:
    static std::vector<Student> read(std::ifstream &inputFile);
    friend void writeNames(std::ostream &os, std::vector<Student> const &students);
    friend void writeNrs(std::ostream &os, std::vector<Student> const &students);

private:
    std::string d_firstName;
    std::string d_lastName;
    std::string d_studentNumber;
    double d_examGrade;
};

#endif