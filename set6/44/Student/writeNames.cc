#include "Student.ih"

void writeNames(ostream &os, vector<Student> const &students)
{
    vector<Student> sortedStudents = students;

    sort(sortedStudents.begin(), sortedStudents.end(),
         [](const Student &alpha, const Student &beta)
         {
             return strcasecmp(alpha.d_lastName.c_str(), beta.d_lastName.c_str()) < 0;
         });

    for (const auto &student : sortedStudents)
    {
        os << "First Name: " << student.d_firstName << "\n";
        os << "Last Name: " << student.d_lastName << "\n";
        os << "Student Number: " << student.d_studentNumber << "\n";
        os << "Exam Grade: " << student.d_examGrade << "\n";
        os << "-----------------------------\n";
    }
}
