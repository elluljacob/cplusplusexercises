#include "main.ih"

int main()
{
    ShowExcepts showExcepts(14, exampleFunction);
    
    try 
    {
        showExcepts.asNoexcept();
    }
    catch (...)
    {
        std::cout << "Exception caught in main\n";
    }
}
