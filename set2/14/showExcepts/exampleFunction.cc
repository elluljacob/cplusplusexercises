#include "showExcepts.ih"

// Function that just throws an exception
void exampleFunction()
{
    throw std::runtime_error("Exception thrown from exampleFunction");
}