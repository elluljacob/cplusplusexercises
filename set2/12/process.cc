#include "main.ih"

int process()
try
{
    Arg &arg = Arg::instance();
    
    // Check if there are no arguments or if -v or -h options were specified
    if (arg.nArgs() == 0 || arg.option('v') || arg.option('h'))
        return 0;

    // Check if all command-line arguments are integral numbers
    for (size_t idx = 0; idx < arg.nArgs(); ++idx)
    {
        try
        {
            // Attempt to convert the argument to an integer
            stoi(arg.arg(idx));
        }
        catch (const std::invalid_argument &)
        {
            // If the conversion fails, throw an exception with the index
            throw int(idx + 1); // Adding 1 to indicate the index of the argument
        }
    }

    // All arguments are integral numbers, print them
    for (size_t idx = 0; idx < arg.nArgs(); ++idx)
        cout << arg.arg(idx) << '\n';

    // Return 0 at the end of the successful execution
    return 0;
}
catch (int idx)
{
    // Handle the exception for non-integral arguments
    cerr << "Error: Non-integer argument at index " << idx << ": " <<      Arg::instance().arg(idx - 1) << '\n';
    return idx;
}
catch (...)
{
    cerr << "An unexpected error occurred in process()\n";
    return 1;
}

