#include "main.ih"

namespace 
{
    char version[] = "1.00.00";
    
    Arg::LongOption longOptions[] = 
    {
        Arg::LongOption{"required", Arg::Required},
        Arg::LongOption{"debug"},
        Arg::LongOption{"filenames", 'f'},
        Arg::LongOption{"help", 'h'},
        Arg::LongOption{"version", 'v'},
    };
    auto longEnd = longOptions + size(longOptions);
}

int main(int argc, char **argv)
try
{
    Arg &arg = Arg::initialize("vhaf:", longOptions, longEnd, argc, argv);

    cout << arg.basename() << '\n';

    arg.versionHelp(usage, version, 1);

    process();  // Call the process function to handle command-line arguments
}
catch (int exitCode)
{
    // Handle the exception thrown by process() and return the specified exit code
    return exitCode;
}
catch (...)
{
    cout << "An unexpected error occurred" << '\n';
    return 1;
}