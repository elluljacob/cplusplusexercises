#ifndef SHOW_EXCEPTS_H
#define SHOW_EXCEPTS_H

class ShowExcepts
{
    int value;
    void (*func)();

public:
    ShowExcepts(int v, void (*f)());

    void asNoexcept();
};

// Example function to pass to the constructor
void exampleFunction();

#endif