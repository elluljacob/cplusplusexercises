#include "showExcepts.ih"

// Mimicks the functionality of noExcept
void ShowExcepts::asNoexcept()
try 
{
    func();
}
catch (...) 
{
    std::cout << "Exception caught in asNoexcept\n";
}
