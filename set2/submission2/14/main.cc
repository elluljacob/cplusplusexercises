// ML: 0
// ML: NAE
// This seems to be last years exercise 14.
// But this year this exercise has been replaced.

// Jacob: Note the issue we discussed in the email for this exercise
#include "main.ih"

int main()
{
    ShowExcepts showExcepts(14, exampleFunction);
    
    try 
    {
        showExcepts.asNoexcept();
    }
    catch (...)
    {
        std::cout << "Exception caught in main\n";
    }
}
