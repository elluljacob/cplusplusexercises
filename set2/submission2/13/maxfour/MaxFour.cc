#include "maxfour.ih"

MaxFour::MaxFour() 
{
    if (count >= 4)
        throw "Max number of objects reached.";
        
    ++count;
    std::cout << "object count: "<< count << '\n';
}
