#include "maxfour/maxfour.ih"
#include "iostream"

int main() 
{   
    try 
    {
        MaxFour *arr =  new MaxFour[4];
        delete [] arr;
    } 
    catch (char const* excep) 
    {
        std::cerr << "Exception caught: " << excep << "\n";
    }
}
