#ifndef PROCESS_H
#define PROCESS_H

#include <iostream>

struct Demo 
{
    public:
        Demo();
        Demo(int value)
        {       //Added try-catch fully inside constructor.
            try
            {
                throw value;
            } 
            catch(int value)
            {
                std::cerr 
                << "Caught exception in constructor. Thrown value: " 
                << value 
                << "\n";
            }
        };

        ~Demo() 
        {
            std::cerr << "Destructor called\n";
        };
};

#endif