#include "main.ih"

void test();    // Shows that a copy is made of an object when caught
void test2();   // Shows that references arent copied
void test3();   // Shows that a copy of an object is thrown
void test4();   // Shows catching an object without reference

int main()
{
    test();
    test2();
    test3();
    test4();
}