#include "main.ih"

int handleException() 
try
{
    throw;  // Re-throw the current exception to handle it
}
catch (int exitCode)
{
    return exitCode;  // Return 0 for -h, 1 for invalid args
}
catch (const std::string &errorMessage)
{
    std::cerr << "Error: " << errorMessage << '\n';
    return 1;  // Return 1 for file-related errors
}
catch (const std::exception &e) // ML: SLV
{
    std::cerr << "Exception: " << e.what() << '\n';
    return 1;  // Return 1 for other standard exceptions
}
catch (...)
{
    std::cerr << "An unknown error occurred\n";
    return 1;  // Returning 1 for unknown errors
}
