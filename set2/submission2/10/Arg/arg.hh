#ifndef ARG_H
#define ARG_H

class Arg
{
        public:
            static Arg const &instance(unsigned argc, char **argv, 
                                       char const *arguments);
};

#endif