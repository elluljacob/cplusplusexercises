#include "maxfour.ih"

int MaxFour::count = 0;

MaxFour::MaxFour() 
{
    if (count >= 4) 
        throw "Max number of objects reached.";
        
    ++count;
}