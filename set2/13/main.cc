#include "main.ih"

int main() {
    MaxFour* maxFourArray = 0;
    try {
        maxFourArray = new MaxFour[5];
    } 
    catch (char const* excep) 
    {
        std::cerr << "Exception caught: " << excep << "\n";
        delete [] maxFourArray;
    }
}