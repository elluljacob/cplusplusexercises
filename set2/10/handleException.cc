#include "main.ih"

int handleException()
{
    try
    {
        throw;  // Re-throw the current exception to handle it
    }
    catch (int exitCode)
    {
        return exitCode;  // Return 0 for -h, 1 for invalidargs
    }
    catch (string const &errorMessage)
    {
        cerr << "Error: " << errorMessage << '\n';
        return 1;  // Return 1 for file-related errors
    }
    catch (exception const &e)
    {
        cerr << "Exception: " << e.what() << '\n';
        return 1;  // Return 1 for other standard exceptions
    }
    catch (...)
    {
        cerr << "An unknown error occurred\n";
        return 1;
    }
}