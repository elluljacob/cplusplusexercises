#ifndef PROCESS_H
#define PROCESS_H

#include "../Arg/arg.hh"

class Process
{
    public:
        Process(Arg const &arg);
        void run();
};   
#endif