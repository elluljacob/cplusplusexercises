#include "main.ih"

void test()
{
    string testString = "This is an unmodified string";
    try
    {
        // Deliberately throw an exception
        throw testString;
    }
    catch (string &exception)
    {
        exception += " that is now modified";
        cout << testString << '\n';
        cout << exception << '\n';      
    }
}
