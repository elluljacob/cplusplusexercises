#include "main.ih"

void test2()
{
    string testString = "This is an unmodified string";
    try
    {
        throw &testString;
    }
    catch (string *exception)
    {
        *exception += " that is now modified";
        cout << testString << '\n';
        cout << *exception << '\n';
    }
}