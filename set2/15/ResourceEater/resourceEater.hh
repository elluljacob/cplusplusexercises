#ifndef RESOURCE_EATER_H
#define RESOURCE_EATER_H
#include <iostream>

void exampleExit();
void exampleException();

class ResourceEater
{
    public:
        ResourceEater();
        ~ResourceEater();
};

inline ResourceEater::ResourceEater() 
{
    std::cout << "Aquiring resources\n";
}
        
inline ResourceEater::~ResourceEater()
{
    std::cout << "Releasing resources\n";
}

#endif