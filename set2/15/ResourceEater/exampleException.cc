#include "resourceEater.ih"

void exampleException()
{
    ResourceEater res;
    cout << "Processing...\n";
    throw runtime_error("Error occured");
}