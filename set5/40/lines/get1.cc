#include "lines.ih"

Lines::containerType const &Lines::get() const
{
    return d_linesData.front();
}