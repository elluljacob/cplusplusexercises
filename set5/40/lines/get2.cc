#include "lines.ih"

Lines::containerType const &Lines::get(size_t idx) const
{
    return d_linesData.at(idx);
}