#ifndef LINES_H
#define LINES_H

#include <vector> // std::vector
#include <string> // std::string
#include <iosfwd> // std::istream

class Lines
{
public:
    using containerType = std::vector<std::string>;

<<<<<<< HEAD
    Lines(std::istream &in);

    containerType const &get() const;
    containerType const &get(size_t idx) const;

private:
    // Static means only 1 time memory allocation during program lifetime, so technically not a datamember?
    // It is defined here rather than the start of the class to be able to use the typedef
    static std::vector<containerType> d_linesData; // Object will have pointers to the data, not actual data member
=======
    Lines(std::istream &in);

    containerType const &get() const;
    containerType const &get(size_t idx) const;

private:
    // Static means only 1 time memory allocation during program lifetime, so technically not a datamember?
    // It is defined here rather than the start of the class to be able to use the typedef
    static std::vector<containerType> d_linesData; // Object will have pointers to the data, not actual data member
>>>>>>> thomas
};

#endif