#include "lines.ih"

vector<Lines::containerType> Lines::d_linesData;

Lines::Lines(istream &in)
{
    string line;
    while (getline(in, line) && !line.empty())
    {
        d_linesData.emplace_back();
        while (!line.empty())
        {
            d_linesData.back().push_back(line);
            if (!getline(in, line))
                break;
        }
    }
}