#include "main.ih"

int main(int argc, char **argv)
{
    ifstream in(argv[1]);

    if (!in)
    {
        cerr << "Can't open file " << argv[1] << '\n';
        return 1;
    }

    Lines lines1{in};
    Lines lines2{in};

    cout << "lines1 lines:\n";
    for (string line : lines1.get())
        cout << line << '\n';

    cout << "lines2 lines:\n";
    for (string line : lines1.get(1))
        cout << line << '\n';
}