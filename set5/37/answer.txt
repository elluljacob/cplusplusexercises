Shrink_to_fit() is not guaranteed to actually reduce the capacity; 
the C++ standard allows implementations to decide whether to honor the request. 
Additionally, there may be a performance overhead associated with this 
operation, as it involves copying ellements to a new memory location.
Moreover, different C++ compilers and libraries may implement shrink_to_fit() 
differently, leading to varied behavior across platforms.
Shrinking the capacity might result in memory fragmentation if the 
freed memory is not contiguous, potentially impacting performance.