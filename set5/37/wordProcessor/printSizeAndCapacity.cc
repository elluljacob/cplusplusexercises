#include "wordProcessor.ih"

void MyWordProcessor::printSizeAndCapacity() 
{
    cout << "Vector size: " << uniqueWords.size() << '\n';
    cout << "Vector capacity: " << uniqueWords.capacity() << '\n';
}