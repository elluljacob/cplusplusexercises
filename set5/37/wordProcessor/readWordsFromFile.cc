#include "wordProcessor.ih"

void MyWordProcessor::readWordsFromFile(string const &fileName)
{
    ifstream file(fileName);

    string word;
    while (file >> word)
    {
        // Store unique words in the vector
        if (find(uniqueWords.begin(), uniqueWords.end(), word) == uniqueWords.end())
            uniqueWords.push_back(word);
    }
}