#include "wordProcessor.ih"

void MyWordProcessor::addAdditionalWord(const string &additionalWord)
{
    uniqueWords.push_back(additionalWord);

    // Swap to shed excess capacity
    vector<string>(uniqueWords).swap(uniqueWords);
}