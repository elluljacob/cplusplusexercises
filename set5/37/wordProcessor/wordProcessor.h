#ifndef WORDPROCESSOR_H
#define WORDPROCESSOR_H

#include <vector>
#include <string>

class MyWordProcessor
{
    std::vector<std::string> uniqueWords;

public:
    // Constructor
    MyWordProcessor();

    // Member function to read words from a file and store in the vector
    void readWordsFromFile(std::string const &fileName);

    // Member function to add an additional word to the vector
    void addAdditionalWord(std::string const &additionalWord);

    // Member function to print size and capacity of the vector
    void printSizeAndCapacity();
};

#endif // WORDPROCESSOR_H
