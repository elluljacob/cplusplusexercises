#include "main.ih"

int main()
{
    ifstream file("input.txt"); // Read unique words from a file
    vector<string> uniqueWords;

    string word;
    while (file >> word) // Store unique words in the vector
    {
        if (find(uniqueWords.begin(), uniqueWords.end(), word) == uniqueWords.end())
            uniqueWords.push_back(word);
    }

    // Print size and capacity of the vector
    cout << uniqueWords.size() << ", " << uniqueWords.capacity() << '\n';
    uniqueWords.push_back("additionalWord"); // Add one additional word

    cout << uniqueWords.size() << ", " << uniqueWords.capacity() << '\n';

    // Create a new vector and swap with the existing one
    vector<string>(uniqueWords).swap(uniqueWords);

    // Print size and capacity after shedding excess capacity
    cout << uniqueWords.size() << ", " << uniqueWords.capacity() << '\n';

    MyWordProcessor wordProcessor;
    wordProcessor.readWordsFromFile("input.txt");
    wordProcessor.printSizeAndCapacity();
    wordProcessor.addAdditionalWord("additionalWord");
    wordProcessor.printSizeAndCapacity();
}
