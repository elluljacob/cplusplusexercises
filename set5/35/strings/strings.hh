#ifndef INCLUDED_STRINGS_
#define INCLUDED_STRINGS_

#include <string>
#include <vector>

class Strings
{
    std::vector<std::string> d_str;

public:
    Strings() = default;
    Strings(int argc, char **argv); // 2.cc
    Strings(char **environLike);    // 3.cc

    Strings &operator+=(std::string const &str);
};

#endif
