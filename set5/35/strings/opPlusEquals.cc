#include "strings.ih"

Strings &Strings::operator+=(string const &str)
{
    d_str.push_back(str);
    return *this;
}