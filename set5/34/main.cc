#include "main.ih"

int main()
{
    map<string, size_t> wordcount; // Declares a map with <word, count>.
    string word;

    while (cin >> word)    // Extract values from the input.
        ++wordcount[word]; // For each word it increments the count.

    for (auto const &it : wordcount)
        cout << it.first << it.second << '\n';
}