#include "address.ih"
#include <sstream>

istream &Address::extract(istream &in)
{
    string line;
    while (getline(in, line))
    {
        stringstream ss(line);
        string postalCode, addressStr, person;

        getline(ss, postalCode, ';');
        getline(ss, addressStr, ';');

        vector<string> people;
        while (getline(ss, person, ';'))
            people.push_back(person); // Followed by adding them to the vector

        d_data[{postalCode, addressStr}] = std::move(people); // fill the data
    }
    return in;
};