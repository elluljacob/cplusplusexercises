#ifndef INCLUDED_ADDRESS_H_
#define INCLUDED_ADDRESS_H_

#include <string>
#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Address
{
    friend istream &operator>>(istream &in, Address &address);
    friend ostream &operator<<(ostream &out, const Address &address);

    struct KeyHash
    {
        size_t operator()(const pair<string, string> &key) const;
    };

    unordered_map<pair<string, string>, vector<string>, KeyHash> d_data;

public:
    size_t remove(string const &postalCode);
    ostream &insert(ostream &out) const;
    istream &extract(istream &out);
};

inline istream &operator>>(istream &in, Address &address)
{
    return address.extract(in);
}

inline ostream &operator<<(ostream &out, const Address &address)
{
    return address.insert(out);
}

inline size_t Address::KeyHash::operator()(const pair<string, string> &key) const
{
    return hash<string>{}(key.first) + hash<string>{}(key.second);
}

#endif