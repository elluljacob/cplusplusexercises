#include "address.hh"

size_t Address::remove(string const &prefix)
{
    size_t removedCount = 0;
    // Using auto to capture the iterator by reference
    auto it = d_data.begin();
    while (it != d_data.end())
    {
        if (it->first.first.find(prefix) != string::npos)
            it = d_data.erase(it);
        else
            ++it;
    }

    return removedCount;
}