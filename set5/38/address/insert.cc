#include "address.ih"

ostream &Address::insert(ostream &out) const
{
    for (const auto &value: d_data) 
    {
        out << value.first.first << "; " << value.first.second;
        for (const string &str: value.second)       
            out << "; " << str;
        out << '\n';
    }
    return out;
};