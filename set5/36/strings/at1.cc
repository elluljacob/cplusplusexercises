#include "strings.ih"

string &Strings::at(size_t idx)
{
    if (idx >= d_str.size())
        throw out_of_range("Index out of range");

    if (d_str[idx].use_count() > 1)
        d_str[idx] = make_shared<string>(*d_str[idx]);

    return *d_str[idx];
}