#include "strings.ih"

string const &Strings::operator[](size_t idx) const
{
    return *d_str.at(idx); // Dereference the shared_ptr to return the string
}