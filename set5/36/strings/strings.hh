#ifndef INCLUDED_STRINGS_
#define INCLUDED_STRINGS_

#include <string>
#include <vector>
#include <memory>

class Strings
{
    using StringPtr = std::shared_ptr<std::string>;
    std::vector<StringPtr> d_str;

public:
    Strings() = default;
    Strings(int argc, char **argv); // 2.cc
    Strings(char **environLike);    // 3.cc

    std::string &at(size_t idx);
    std::string const &at(size_t idx) const;

    Strings &operator+=(std::string const &str);
    std::string &operator[](size_t idx);
    std::string const &operator[](size_t idx) const;
};

#endif
