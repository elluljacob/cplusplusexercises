#include "strings.ih"

string const &Strings::at(size_t idx) const
{
    if (idx >= d_str.size())
        throw out_of_range("Index out of range");

    return *d_str.at(idx);
}