#include "strings.ih"

Strings &Strings::operator+=(string const &str)
{
    d_str.push_back(make_shared<string>(str));
    return *this;
}