#include "strings.ih"

string &Strings::operator[](size_t idx)
{
    // If the string is shared with others (use_count > 1), create a new copy
    if (d_str[idx].use_count() > 1)
        d_str[idx] = make_shared<string>(*d_str[idx]);

    return *d_str[idx]; // Dereference the shared_ptr to return the string
}