#include "main.ih"

int main()
try
{
        Strings str;
        str += "Hello";
        str += "World";

        cout << "Current Strings:" << '\n';
        for (size_t idx = 0; idx != 2; ++idx)
                cout << "String " << idx << ": " << str[idx] << '\n';

        string first = str[0];
        str[1] = "There";

        cout << "First string: " << first << '\n';
        cout << "Modified second string: " << str[1] << '\n';

        // Access an index out of range
        cout << "Accessing third string: " << str.at(2);
}
catch (out_of_range const &exception)
{
        cout << "Exception caught: " << exception.what() << '\n';
}
