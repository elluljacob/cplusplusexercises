#include "main.ih"

int main()
{
    set<string> uniqueWords; // Container to store unique words
    string word;

    while (cin >> word) // Extracting and adding to set
        uniqueWords.insert(word);

    // Print sorted list of unique words
    for (auto const &word : uniqueWords)
        cout << word << '\n';
}