#include "derived.ih"

Derived::Derived(Derived const &other)  : Base::Base(other)
{
    cout << "Derived copy constructor\n";
}