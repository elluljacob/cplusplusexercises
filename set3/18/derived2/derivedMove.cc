#include "derived.ih"

Derived::Derived(Derived &&other) : Base::Base(move(other))
{
    cout << "Derived move constructor\n";
}