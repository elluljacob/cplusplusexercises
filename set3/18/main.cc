#include "main.ih"

int main()
{
    Derived d1;                     // Default
    Derived d2(d1);                 // Copy
    Derived d3(std::move(d1));      // Move
}
