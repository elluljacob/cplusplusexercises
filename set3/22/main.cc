#include "main.ih"

int main() 
{
    Base b;
    Derived d;

    caller(b); //calls Base::hello
    caller(d); //calls Derived::hello due to polymorphism
}
