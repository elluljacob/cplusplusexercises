#include "main.ih"

void caller(Base &obj)
{
    // Cast to derived object
    Derived* derivedObj = dynamic_cast<Derived*>(&obj);
    derivedObj->hello();
}