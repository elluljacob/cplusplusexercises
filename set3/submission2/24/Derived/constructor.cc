#include "derived.ih"

Derived::Derived(std::string const &name)
:
Base(fileStream),
fileStream(name)
{
    try
    {
        if (!fileStream.is_open()) 
            throw std::exception();
    }
    catch (const std::exception& except)
    {
        std::cerr << "Exception in Derived class: " << except.what() << '\n';
    }
};