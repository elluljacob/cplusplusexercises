#ifndef DERIVED_H
#define DERIVED_H

#include <fstream>
#include <string>
#include "../Base/base.hh"

class Derived : public Base
{
    std::ofstream fileStream;
    public:
        Derived(std::string const &name);
};

#endif