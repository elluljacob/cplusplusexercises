#ifndef STRING_H
#define STRING_H

#include <string>

class String : public std::string 
{
    public:
        using std::string::string;  // Inherit constructors of std::string
        
        // Bring all overloads of std::string::insert into scope
        using std::string::insert;
        
        // Member function to insert text at the beginning
        String &insert(std::string const &txt);
};

// Comparison operators
bool operator==(String const& lhs, String const& rhs);
bool operator!=(String const& lhs, String const& rhs);
bool operator<(String const& lhs, String const& rhs);
bool operator<=(String const& lhs, String const& rhs);
bool operator>(String const& lhs, String const& rhs);
bool operator>=(String const& lhs, String const& rhs);

inline bool operator==(String const& lhs, String const& rhs) 
{ 
    return static_cast<std::string>(lhs) == rhs;
}

inline bool operator!=(String const& lhs, String const& rhs) 
{ 
    return static_cast<std::string>(lhs) != rhs;
}

inline bool operator>=(String const& lhs, String const& rhs) 
{ 
    return static_cast<std::string>(lhs) >= rhs; 
}

inline bool operator>(String const& lhs, String const& rhs) 
{ 
    return static_cast<std::string>(lhs) > rhs;
}

inline bool operator<=(String const& lhs, String const& rhs) 
{ 
    return static_cast<std::string>(lhs) <= rhs;
}

inline bool operator<(String const& lhs, String const& rhs) 
{ 
    return static_cast<std::string>(lhs) < rhs;
}
#endif
