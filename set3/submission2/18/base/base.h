#ifndef BASE_H
#define BASE_H

class Base
{
    public:
        Base();
        Base(Base const &other);
        Base(Base &&other);
};

#endif