#ifndef DERIVED_H
#define DERIVED_H

#include "../base/base.h"

class Derived : public Base
{
    public:
        Derived();
        Derived(Derived const &other);
        Derived(Derived &&other);
};

#endif