#include "main.ih"

int main()
{
    // Show improper way
    Derived d1;                     // Default
    Derived d2(d1);                 // Copy
    Derived d3(std::move(d1));      // Move

    // Use proper base constructor
    Derived2 d1v2;                     // Default
    Derived2 d2v2(d1v2);                 // Copy
    Derived2 d3v2(std::move(d1v2));      // Move
}
