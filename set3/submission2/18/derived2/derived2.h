#ifndef DERIVED2_H
#define DERIVED2_H

#include "../base/base.h"

class Derived2 : public Base
{
    public:
        Derived2();
        Derived2(Derived2 const &other);
        Derived2(Derived2 &&other);
};

#endif