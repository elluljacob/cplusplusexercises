#include "derived2.ih"

Derived2::Derived2(Derived2 const &other)  
    : Base(other)
{
    cout << "Derived copy constructor\n";
}
