#include "derived2.ih"

Derived2::Derived2(Derived2 &&other) 
    : Base(move(other))
{
    cout << "Derived move constructor\n";
}