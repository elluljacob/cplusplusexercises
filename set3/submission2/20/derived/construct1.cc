#include "derived.ih"

Derived::Derived(size_t count, Derived const &str)
{
    reserve(count * str.size()); //reallocates memory once
    for (size_t idx = 0; idx != count; ++idx)
        insert(end(), str.begin(), str.end()); //reallocs on insufficent memory
    
};