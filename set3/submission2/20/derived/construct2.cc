#include "derived.ih"

Derived::Derived(size_t count, Derived *const str) 
{
    if (str) 
    {
        reserve(count * (*str).size()); //reserves memory
        for (size_t idx = 0; idx != count; ++idx) 
            insert(end(), (*str).begin(), (*str).end());
    }
}