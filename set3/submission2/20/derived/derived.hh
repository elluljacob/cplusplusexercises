#ifndef DERIVED_H
#define DERIVED_H

#include <string>

class Derived : public std::string 
{
    public:
        using std::string::string;

    Derived(size_t count, Derived const &str);
    Derived(size_t count, Derived *const str);
};
#endif

