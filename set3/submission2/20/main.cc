#include "main.ih"

int main()
{
    size_t count = 3;
    Derived str = "reference ";
    Derived value(count, "value "); //Value
    Derived reference(count, str); //Reference
    Derived rvalue(count, std::move(str)); //RValue reference
    Derived* ptr = new Derived(count, "pointer "); //Pointer
    cout << value << '\n';
    cout << reference << '\n';
    cout << rvalue << '\n';
    cout << *ptr << '\n';
}
