#include <iostream>
#include <string>

class Derived : public std::string 
{
    public:
        using std::string::string;

        Derived(size_t count, Derived const &str)
        {
            for(size_t idx = 0; idx != count; ++idx)
                append(str);
            
        };

        Derived(size_t count, const Derived* str) 
        {
        if (str) 
            for (size_t idx = 0; idx != count; ++idx) 
                append(*str);  
        }
};