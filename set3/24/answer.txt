By having the Base class that isn't ours, we try to match the input to its constructor.
For this exercise we have a Derived class with the constructor Derived(std::string const &name);
the goal is to submit the class interface of the class Derived and submit the constructor's 
implementation, as well as succinctly explain why the implementation is correct.

By doing Derived(std::string const &name) : Base(fileStream), fileStream(name)
we firstly initialize the fileStream object with the file name and then pass
it on to the Base class, before finishing the construction of the object we
do a try-catch to see if the file is open and else we throw an exception.