#include <iostream>
#include <fstream>
#include <string>
#include "../Base/base.hh"

class Derived : public Base 
{
    std::ofstream fileStream;

    public:
        Derived(std::string const &name)
        :
            Base(fileStream),
            fileStream(name)
        {
            try
            {
                if (!fileStream.is_open()) 
                    throw std::string("Failed to open file: " + name);
                
            }
            catch(const std::string& e)
            {
                std::cerr 
                << "Exception in Derived class: " 
                << e
                << '\n';
            }
        };
};