#include "main.ih"

int main() 
{
    String str("Hello");

    // Using insert member function
    str.insert("World");
    cout << "After insertion: " << str <<  "\n" ;

    // Using comparison operators
    String str2("WorldHello");
    cout <<  boolalpha;
    cout << "str == str2: " << (str == str2) << "\n";
    cout << "str != str2: " << (str != str2) <<  "\n" ;
    cout << "str < str2: " << (str < str2) <<  "\n" ;
    cout << "str <= str2: " << (str <= str2) <<  "\n" ;
    cout << "str > str2: " << (str > str2) <<  "\n" ;
    cout << "str >= str2: " << (str >= str2) <<  "\n" ;
}
