#ifndef STRING_H
#define STRING_H

#include <string>

class String : public std::string 
{
    public:
        // Constructor
        using std::string::string;  // Inherit constructors of std::string

        // Member function to insert text at the beginning
        String &insert(std::string const &txt);

        // Binary comparison operators
        bool operator==(String const& rhs) const { return compare(rhs) == 0; }
        bool operator!=(String const& rhs) const { return compare(rhs) != 0; }
        bool operator<(String const& rhs) const { return compare(rhs) < 0; }
        bool operator<=(String const& rhs) const { return compare(rhs) <= 0; }
        bool operator>(String const& rhs) const { return compare(rhs) > 0; }
        bool operator>=(String const& rhs) const { return compare(rhs) >= 0; }
};

#endif 