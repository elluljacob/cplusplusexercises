#include "multiOut.ih"

// starts each thread and then collects and outputs the results
void MultiOut::run()
{
    vector<thread> threads;
    for (int threadId = 0; threadId < num_threads; ++threadId)
        threads.emplace_back(&MultiOut::thread_function, this, threadId);
    for (auto &thread : threads)
        thread.join();
    // Output the stored strings in an ordered manner
    for (auto &output : thread_outputs)
        cout << output;
}