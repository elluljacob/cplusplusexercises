#include "main.ih"

int main(int argc, char *argv[])
{
    const string filename = argv[1];
    const string text = argv[2];

    ofstream output_file(filename);
    //Defines handler and passes it to a thread
    Handler handler;
    thread t1(&Handler::shift, &handler, ref(output_file), ref(text));
    t1.join();

    //Running a thread that defines a handler using lambda
    thread t2([&output_file, &text]()
    {
        Handler handler;
        handler.shift(output_file, text);
    });
    t2.join();
    
    output_file.close();
}