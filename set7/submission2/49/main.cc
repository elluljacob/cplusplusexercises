#include "main.ih"

int main(int argc, char *argv[])
{
    size_t num_primes = stoul(argv[1]);
    PrimeCalculator calculator(num_primes);

    auto start_time = high_resolution_clock::now();
    thread dot_thread(&PrimeCalculator::print_dots, &calculator);
    calculator.calculatePrimes();
    dot_thread.join();
    auto end_time = high_resolution_clock::now();

    for (size_t prime : calculator.getPrimes())
        cout << prime << " ";
    cout << "\n";

    calculator.printTimeInfo(start_time, end_time, num_primes);
}
