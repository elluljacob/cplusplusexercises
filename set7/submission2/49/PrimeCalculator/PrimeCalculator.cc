#include "PrimeCalculator.ih"

PrimeCalculator::PrimeCalculator(size_t num)
    : num_primes(num),
      calculating(true)
{
    primes.push_back(2); // starting with the first prime
}
