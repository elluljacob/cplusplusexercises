#include "PrimeCalculator.ih"

// Method that calculates primes using sieve algorithm
void PrimeCalculator::calculatePrimes()
{
    size_t next = 3;
    while (primes.size() < num_primes)
    {
        if (isPrime(next))
        {
            lock_guard<mutex> guard(prime_mutex);
            primes.push_back(next);
        }
        next += 2; // only check odd numbers
    }
    calculating = false;
}
