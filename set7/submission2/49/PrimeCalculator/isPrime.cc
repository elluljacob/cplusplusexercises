#include "PrimeCalculator.ih"

bool PrimeCalculator::isPrime(size_t number)
{
    for (size_t prime : primes)
    {
        if (number % prime == 0)
            return false;
    }
    return true;
}