#ifndef PRIME_CALCULATOR_HH
#define PRIME_CALCULATOR_HH

#include <mutex>
#include <vector>
#include <chrono>

class PrimeCalculator
{
    std::vector<size_t> primes;
    std::mutex prime_mutex;
    size_t num_primes;
    bool calculating;

public:
    PrimeCalculator(size_t num_primes);
    void calculatePrimes();
    std::vector<size_t> const &getPrimes() const;
    void print_dots();
    void printTimeInfo(std::chrono::time_point<std::chrono::high_resolution_clock> const &start,
                       std::chrono::time_point<std::chrono::high_resolution_clock> const &end,
                       size_t num_primes);

private:
    bool isPrime(size_t number);
};

#endif