#include "PrimeCalculator.ih"

void PrimeCalculator::print_dots()
{
    while (calculating)
    {
        this_thread::sleep_for(chrono::seconds(1));
        cerr << ".";
    }
}