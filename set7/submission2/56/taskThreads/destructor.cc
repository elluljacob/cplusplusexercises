#include "taskThreads.ih"

TaskThreads::~TaskThreads()
{
    for (auto &thread : threads)
    {
        if (thread.joinable())
                thread.join();
    }
};