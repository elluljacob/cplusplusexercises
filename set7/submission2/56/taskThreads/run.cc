#include "taskThreads.ih"

void TaskThreads::run()
{
    startTime = std::chrono::high_resolution_clock::now();
    if (runSequential)
    {   //Runs task sequentially
        for (auto &task: tasks)
            task.countOccurrences();
    } 
    else
    {   //Runs tasks in parallel
        for (auto &task: tasks)
            threads.push_back(thread(&Task::countOccurrences, ref(task)));

        for (auto& thread: threads)
            thread.join();
    }

    for(auto &task: tasks)  //Prints the label and count
        std::cout << task.getLabel() << ": "<< task.getCount() << "\n";
        
    endTime = std::chrono::high_resolution_clock::now();
};