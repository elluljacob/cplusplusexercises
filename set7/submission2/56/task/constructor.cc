#include "task.ih"

Task::Task(std::function<bool(char)> selectFunction, const char* lab):
    selectionFunction(selectFunction),
    label(lab),
    count(0) 
    {
    };
