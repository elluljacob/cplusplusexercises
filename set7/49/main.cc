#include "main.ih"

vector<size_t> primes;
mutex prime_mutex;
bool calculating = true;

int main(int argc, char *argv[])
{
    size_t num_primes = stoul(argv[1]);

    auto start_time = high_resolution_clock::now(); // Record start time
    thread dot_thread(print_dots);                  // Start the dot thread
    calculate_primes(num_primes);                   // Calculate primes in the main thread
    dot_thread.join();                              // Once primes are done stop the dot thread
    auto end_time = high_resolution_clock::now();   // And list end time

    for (size_t prime : primes) // Display the primes
        cout << prime << " ";
    cout << "\n";

    printTimeInfo(start_time, end_time, num_primes);
}
