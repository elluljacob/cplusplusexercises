#include "main.ih"

void printTimeInfo(time_point<high_resolution_clock> const &start,
                   time_point<high_resolution_clock> const &end,
                   size_t num_primes)
{
    time_t start_time_t = system_clock::to_time_t(system_clock::now() - (end - start));
    time_t end_time_t = system_clock::to_time_t(system_clock::now());
    auto duration = duration_cast<seconds>(end - start).count();

    // Display time information
    cout << "Starting time: " << put_time(localtime(&start_time_t), "%a %b %e %H:%M:%S %Y") << '\n';
    cout << "Ending time:   " << put_time(localtime(&end_time_t), "%a %b %e %H:%M:%S %Y") << '\n';
    cout << "Computation of " << num_primes << " primes took " << duration << " seconds\n";
}