#include "main.ih"

// Method that calculates primes using sieve algorithm
void calculate_primes(size_t num_primes)
{
    size_t next = 2;
    while (primes.size() < num_primes)
    {
        bool is_prime = true;
        {
            lock_guard<mutex> guard(prime_mutex);
            for (size_t prime : primes)
            {
                if (next % prime == 0)
                {
                    is_prime = false;
                    break;
                }
            }
        }
        if (is_prime)
        {
            lock_guard<mutex> guard(prime_mutex);
            primes.push_back(next);
        }
        ++next;
    }
    calculating = false;
}
