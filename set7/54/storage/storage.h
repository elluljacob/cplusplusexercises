#ifndef STORAGE_H
#define STORAGE_H

#include <string>
#include <mutex>
#include <queue>
#include <condition_variable>
#include <fstream>

class Storage
{
    std::ofstream d_outfile;
    bool d_finished;
    std::mutex d_mutexQueue;
    std::mutex d_mutexFile;
    std::queue<std::string> d_dataQueue;
    std::condition_variable d_dataCondition;

    public:
        Storage(std::string const &filename);
        ~Storage();
        void push(std::string const &line);
        std::string pop();
        void setFinished();
        bool empty();
        void write(std::string const &line);
        bool isFinished();
};

#endif