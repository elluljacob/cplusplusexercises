#include "storage.ih"

void Storage::setFinished()
{
    lock_guard<mutex> lock(d_mutexQueue);
    d_finished = true;
    d_dataCondition.notify_one();
}