#include "storage.ih"

bool Storage::isFinished()
{
    lock_guard<mutex> lock (d_mutexQueue);
    return d_finished;
}