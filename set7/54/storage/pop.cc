#include "storage.ih"

string Storage::pop()
{
    unique_lock<mutex> lock(d_mutexQueue);

    while (d_dataQueue.empty() && !d_finished)
        d_dataCondition.wait(lock);

    if (!d_dataQueue.empty())
    {
        string line = d_dataQueue.front();
        d_dataQueue.pop();
        return line;
    }

    return "";
}