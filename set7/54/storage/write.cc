#include "storage.ih"

void Storage::write(string const &line)
{
    lock_guard<mutex> lock(d_mutexFile);
    d_outfile << line << endl;
    d_outfile.flush();
}