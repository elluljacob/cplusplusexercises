#include "storage.ih"

bool Storage::empty()
{
    lock_guard<mutex> lock(d_mutexQueue);
    return d_dataQueue.empty();
}