#include "storage.ih"

void Storage::push(string const &line)
{
    unique_lock<mutex> lock(d_mutexQueue);
    d_dataQueue.push(line);
    d_dataCondition.notify_one();
}