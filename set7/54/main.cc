#include "main.ih"

int main()
{
    string filename = "output.txt";
    Storage storage(filename);

    thread consumer(consumerThread, ref(storage));

    string line;
    while (getline(cin, line))
        storage.push(line);

    storage.setFinished();
    consumer.join();
}
