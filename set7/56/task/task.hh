#ifndef TASK_H
#define TASK_H

#include <string>
#include <functional>

class Task
{
private:
    std::function<bool(char)> selectionFunction;
    std::string fileName;
    char const *label;
    int count;

public:
    Task(std::function<bool(char)> selectionFunction, char const *label);
    void setFileName(std::string const &fileName);
    void countOccurrences();
    int getCount();
    char const *getLabel();
};

#endif