#include "taskThreads.ih"

void TaskThreads::showTime(){
    double dur = std::chrono::duration<double>(endTime - startTime).count();
    std::cout << "Duration:   " << dur << " seconds." << "\n";
};