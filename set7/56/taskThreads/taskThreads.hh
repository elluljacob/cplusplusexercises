#ifndef TASKTHREAD_H
#define TASKTHREAD_H

#include "../task/task.hh"
#include <vector>
#include <string>
#include <thread>

class TaskThreads
{
private:
    bool runSequential;
    std::string fileName;
    std::vector<Task> tasks;
    std::vector<std::thread> threads;
    std::chrono::high_resolution_clock::time_point startTime;
    std::chrono::high_resolution_clock::time_point endTime;
    static bool isPunctuation(char ch);

public:
    TaskThreads(int argc, char *argv[]);
    ~TaskThreads();
    void run();
    void showTime();
};

#endif