#include "taskThreads.ih"

bool TaskThreads::isPunctuation(char ch) 
{
    return isprint(ch) && !isalnum(ch) && !isspace(ch);
}