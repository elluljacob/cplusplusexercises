#include "main.ih"

minutes refineToMinutes(auto const &in)
{
    return duration_cast<minutes>(in);
}

int main(int argc, char* argv[])
{
    if (argc < 3)
    {
        cout << "Usage: " << argv[0] << "[hours to mins] [secs to mins]";
        return 1;
    }

    // Hours to minutes
    hours const coarseHours = hours{stoi(argv[1])};
    minutes const mins = refineToMinutes(coarseHours);

    cout << "Coarse hours: " << coarseHours.count() << " hours\n";
    cout << "Fine Minutes: " << mins.count() << " minutes\n";

    // Seconds to minutes
    seconds const coarseSeconds = seconds{stoi(argv[2])};
    minutes const mins2 = refineToMinutes(coarseSeconds);

    cout << "Coarse seconds: " << coarseSeconds.count() << " seconds\n";
    cout << "Fine Minutes: " << mins2.count() << " minutes\n";
}