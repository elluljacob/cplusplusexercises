#include "handler.ih"

void Handler::shift(std::ostream &out, std::string const &text)
{
    std::string modifiedText = text; // Make a local copy for manipulation
    for (size_t idx = 0; idx < text.length(); ++idx)
    {
        out << modifiedText << "\n";
        // shift first letter to the last position
        modifiedText = modifiedText.substr(1) + modifiedText[0];
    }
}