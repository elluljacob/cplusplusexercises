#include "multiOut.ih"

// Function executed by each thread that generates output.
void MultiOut::thread_function(int thread_id)
{
    ostringstream threadOutput;
    for (int lineCount = 0; lineCount < 4; ++lineCount)
    {
        // Direct output to cout (mangled)
        {
            lock_guard<mutex> lock(cout_mutex);
            cout << "thread " << thread_id << '\n';
        }
        // Store output in a string stream (orderly)
        threadOutput << "Line " << lineCount << " from thread " << thread_id << '\n';
        this_thread::yield(); // simulate work and allow other threads to run
    }
    thread_outputs[thread_id] = threadOutput.str();
}

// ES: I accept -> you could have used -> "osyncstream" instead of ostringstream