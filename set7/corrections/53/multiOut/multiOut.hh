#ifndef MULTI_OUT_HH
#define MULTI_OUT_HH

#include <vector>
#include <mutex>
#include <string>

class MultiOut
{
    int num_threads;    // ES: size_t?
    std::vector<std::string> thread_outputs;
    std::mutex cout_mutex;

public:
    MultiOut(std::string num);
    void run();

private:
    void thread_function(int thread_id);
};

#endif
