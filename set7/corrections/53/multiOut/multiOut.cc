#include "multiOut.ih"

// Class constructor
MultiOut::MultiOut(string num)
    : num_threads(stoi(num)),
      thread_outputs(num_threads)
{
}
