// ES: 0
#include "main.ih"

vector<size_t> primes;     // ES: Use less globals -> can do without this global 
mutex prime_mutex;          // ES: Use less globals -> can do without this global 
bool calculating = true;    // ES: This is fine (you can keep it) -> but there exists even a better way if you want the implementation without any globals

int main(int argc, char *argv[])
{
    size_t num_primes = stoul(argv[1]);

    auto start_time = high_resolution_clock::now(); // Record start time
    thread dot_thread(print_dots);                  // Start the dot thread
    calculate_primes(num_primes);                   // Calculate primes in the main thread
    dot_thread.join();                              // Once primes are done stop the dot thread
    auto end_time = high_resolution_clock::now();   // And list end time

    for (size_t prime : primes) // Display the primes
        cout << prime << " ";
    cout << "\n";

    printTimeInfo(start_time, end_time, num_primes);
}
