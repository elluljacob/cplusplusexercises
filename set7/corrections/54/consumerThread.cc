#include "main.ih"

void consumerThread(Storage &storage)
{
    while (true)
    {
        string line = storage.pop();
        if (line.empty())
        {
            if (storage.isFinished() && storage.empty())
                break;
        }
        if (!line.empty())
        {
            storage.write(line);    // ES: PERL
        }
    }
}