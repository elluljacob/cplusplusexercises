#include "storage.ih"

Storage::~Storage()
{
    // Notify consumer thread that main program has finished
    setFinished();
    d_outfile.close();
}