// ES: 1
#include "main.ih"

int main(int argc, char *argv[])
{
    // Parse the argument
    string durationString = argv[1];
    char unit = durationString.back();
    durationString.pop_back();
    int durationValue = stoi(durationString);

    // Get the current time
    auto currentTime = chrono::system_clock::now();

    // Adjust time based on the specified unit and duration
    if (unit == 's')
        currentTime += chrono::seconds(durationValue);
    else if (unit == 'm')
        currentTime += chrono::minutes(durationValue);
    else if (unit == 'h')
        currentTime += chrono::hours(durationValue);
// ES: L
    else
    {
        cerr << "Invalid unit. Use s, m, or h." << '\n';
        return 1;
    }

    // Convert to time_t for gmtime
    time_t currentTime_t = chrono::system_clock::to_time_t(currentTime);

    // Display local time
    cout << "Local Time: " << put_time(localtime(&currentTime_t), "%Y-%m-%d %H:%M:%S") << '\n';

    // Display universal coordinated time (UTC)
    cout << "UTC Time: " << put_time(gmtime(&currentTime_t), "%Y-%m-%d %H:%M:%S") << '\n';
}