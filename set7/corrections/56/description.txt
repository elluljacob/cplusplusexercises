point of this exercise is to pass reusable objects
to threads, with running either sequentially or in
parallel, with the condition that only Task object
can be used for multithreading.