#include "task.ih"

void Task::countOccurrences()
{
    std::ifstream file(fileName);

    if (!file.is_open())
    {
        std::cerr << "Error opening file: " << fileName << "\n";
        return;
    }
    //Lambda function for brevity
    count = count_if(istreambuf_iterator<char>(file), istreambuf_iterator<char>(),
     [&](char character)
     {
        return selectionFunction(character);
     });
     
    file.close();
};