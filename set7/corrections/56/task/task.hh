#ifndef TASK_H
#define TASK_H

#include <string>
#include <functional>

class Task
{
private:
    std::function<bool(char)> selectionFunction; // ES: Avoid bool()
    std::string fileName;
    char const *label;
    int count;  // ES: size_t??

public:
    Task(std::function<bool(char)> selectionFunction, char const *label); // ES: Avoid bool()
    void setFileName(std::string const &fileName);
    void countOccurrences();
    int getCount();
    char const *getLabel();
};

#endif