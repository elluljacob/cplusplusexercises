#include "taskThreads.ih"
// ES: INCONS{}
void TaskThreads::showTime(){
    double dur = std::chrono::duration<double>(endTime - startTime).count();
    std::cout << "Duration:   " << dur << " seconds." << "\n";
};