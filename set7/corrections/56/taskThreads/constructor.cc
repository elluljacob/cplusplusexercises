#include "taskThreads.ih"

TaskThreads::TaskThreads(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cerr << "Usage: " << argv[0] << " <filename> [args]" << "\n";
        return;
    }

    fileName = argv[1];
    runSequential = (argc > 2);

    tasks.push_back(Task(isalpha, "vowels"));
    tasks.push_back(Task(isdigit, "digits"));
    tasks.push_back(Task(isxdigit, "hexadecimal digits"));
    tasks.push_back(Task(isPunctuation, "punctuation characters"));

    for (auto &task : tasks)
    {
        task.setFileName(fileName); // ES: PERL
    }
};