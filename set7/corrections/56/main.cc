// ES: ?
#include "main.ih"

int main(int argc, char *argv[])
{
    TaskThreads taskThreads(argc, argv);
    taskThreads.run();
    taskThreads.showTime();
}

// ES: put main in try catch